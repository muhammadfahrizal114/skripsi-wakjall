<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH DATA HARGA KEBUN</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah Harga</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
										    <label>Kebun</label>
											<select name="kebun" id="kebun" class="form-control">
												<option>--Pilih Kebun--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM kebun");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_blok']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
                                            <label>Harga Bagus</label>
                                            <input type="text" class="form-control" name="hbg"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Harga Kuranga Bagus</label>
                                            <input type="text" class="form-control" name="hkb"/>
                                          </div>
                                          
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="harga.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $kebun=$_POST['kebun'];
                                      $hbg=$_POST['hbg'];
                                      $hkb=$_POST['hkb'];
                                      

                                      if(isset($kebun,$hbg)){
                                        if((!$hbg)||(!$kebun)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO harga VALUES ('','$kebun','$hbg','$hkb',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=kebun.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>