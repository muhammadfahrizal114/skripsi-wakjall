<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
         

        <ul id="sidebarnav">
            
            <li> <a class="waves-effect waves-dark" href="halutama.php" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
				
            </li>
            
            <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-desktop-mac"></i><span class="hide-menu">Aktifitias</span></a>
				<ul>
				<li> <a class="waves-effect waves-dark" href="cuaca.php" aria-expanded="false"></i><span class="hide-menu">Cuaca</span></a>
				<li> <a class="waves-effect waves-dark" href="hama.php" aria-expanded="false"></i><span class="hide-menu">Hama</span></a>
				<li> <a class="waves-effect waves-dark" href="pupuk.php" aria-expanded="false"></i><span class="hide-menu">Pupuk</span></a>
				</ul>
            </li>
            
            <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-wallet"></i><span class="hide-menu">Kelola Keuangan</span></a>
				<ul>
				<li> <a class="waves-effect waves-dark" href="pemasukan.php" aria-expanded="false"></i><span class="hide-menu">Pemasukan</span></a>
				<li> <a class="waves-effect waves-dark" href="pengeluaran.php" aria-expanded="false"></i><span class="hide-menu">Pengeluaran</span></a>
				</ul>
            </li>
            
            <li> <a class="waves-effect waves-dark" href="user.php" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">User</span></a>
            </li>
            
            <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Inventory</span></a>
			    <ul>
				<li> <a class="waves-effect waves-dark" href="permintaan.php" aria-expanded="false"></i><span class="hide-menu">Permintaan</span></a>
				<li> <a class="waves-effect waves-dark" href="aset.php" aria-expanded="false"></i><span class="hide-menu">Aset</span></a>
				<li> <a class="waves-effect waves-dark" href="pemakaian.php" aria-expanded="false"></i><span class="hide-menu">Pemakaian</span></a>
				</ul>
            </li>
			
			<li> <a class="waves-effect waves-dark" href="hasil.php" aria-expanded="false"><i class="mdi mdi-tag"></i><span class="hide-menu">Hasil Panen</span></a>
            </li>
			
			<li> <a class="waves-effect waves-dark" href="laporan.php" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Laporan</span></a>
            </li>
			
			<li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Setup</span></a>
				<ul>
				<li> <a class="waves-effect waves-dark" href="kebun.php" aria-expanded="false"></i><span class="hide-menu">Kebun</span></a>
				<li> <a class="waves-effect waves-dark" href="harga.php" aria-expanded="false"></i><span class="hide-menu">Harga</span></a>
				</ul>
            </li>
			
            <li> <a class="waves-effect waves-dark" href="profile.php?id_=<?php echo $_SESSION['id_']; ?>" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">Profil Akun</span></a>
            </li>
			
            <li> <a class="waves-effect waves-dark" href="logout.php" aria-expanded="false"><i class="mdi mdi-logout"></i><span class="hide-menu">Logout</span></a>
            </li>
            
            
        </ul>   
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            
        </aside>