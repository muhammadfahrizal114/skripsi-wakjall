<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Permintaan</li>
                        </ol>
                    </div>
                   
                </div>
                
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="permintaanTambah.php">Tambah Permintaan</a></p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode Permintaan</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah (Pcs)</th>
											<th>Harga</th>
											<th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM permintaan ORDER BY id DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['permintaan'];?></td>
                                            <td><?php echo $row['nama_barang'];?></td>
                                            <td><?php echo $row['jumlah'];?></td>
											<td><?php echo $row['harga'];?></td>
											<td>
											<?php 
										    if($row['status']=='1'){
											echo "<a class='btn btn-primary' href='#'>
                                                Diajukan
                                                </a>";
											}elseif($row['status']=='2'){
											echo "<a class='btn btn-success' href='#'>
                                                Disetujui
                                                </a>";
											}else{
											echo "<a class='btn btn-danger' href='#'>
                                                Ditolak
                                                </a>";
											}
											?>
											</td>
                                        <?php
											
											 
											   print("
											    <td>
                                                <a class='btn btn-danger' href='permintaanDelete.php?id_=$row[id]'>
                                                <i class='mdi mdi-delete'></i>
                                                </a>
                                                </td>
                                              </tr>");
											                                                

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>