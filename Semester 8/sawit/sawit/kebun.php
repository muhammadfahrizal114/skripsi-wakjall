<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Kebun</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="kebunTambah.php">Tambah Data</a></p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kebun</th>
                                            <th>Panjang</th>
                                            <th>Lebar</th>
											<th>Luas</th>
											<th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM kebun ORDER BY id DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['nama_kebun'];?></td>
                                            <td><?php echo $row['panjang'];?></td>
                                            <td><?php echo $row['lebar'];?></td>
											<td><?php echo $row['luas'];?></td>
											<td><?php echo $row['keterangan'];?></td>

                                        <?php

                                              
                                              print("
                                                <td>
												<a class='btn btn-primary' href=blok.php?id_=$row[id]>
                                                Tambah Blok
                                                </a>
                                                <a class='btn btn-warning' href=kebunEdit.php?id_=$row[id]>
                                                Ubah
                                                </a>
                                                <a class='btn btn-danger' href='kebunDelete.php?id_=$row[id]'>
                                                Hapus
                                                </a>
                                                </td>
                                              </tr>");
                                              

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>