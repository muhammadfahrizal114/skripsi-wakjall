<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Pemasukan Lainnya</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Pemasukan Lainnya</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
				  <?php
					$char = date("Ymd");
					$query=mysqli_query($konek,"SELECT max(permintaan) as max_kode FROM permintaan 
					WHERE permintaan LIKE '{$char}%' ORDER BY permintaan DESC LIMIT 1");
					$data = mysqli_fetch_array($query);
					$kodeTransaksi = $data['max_kode'];
					$no = substr($kodeTransaksi, -3, 3);
					$no = (int) $no;
					$no += 1;
					$newKodeTransaksi = $char .'/'. sprintf("%03s", $no);
				   ?>
										  <div class="form-group">
										    <label>Kode Permintaan</label>
                                            <input type="text" class="form-control" name="kd" value="PRM/<?php echo $newKodeTransaksi; ?>" readonly />
                                          </div>
										  <div class="form-group">
										    <label>Nama Barang</label>
											<input type="hidden" name="id_" value="<?php echo $_SESSION['id_']; ?>" />
                                            <input type="text" class="form-control" name="brg" />
                                          </div>
										  <div class="form-group">
										    <label>Jumlah (pcs)</label>
                                            <input type="text" class="form-control" name="jml" />
                                          </div>
										  <div class="form-group">
										    <label>Harga</label>
                                            <input type="text" class="form-control" name="hrg" />
                                          </div>
                                          <div class="form-group">
										    <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" cols="10"></textarea>
                                          </div>
							
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="pengeluaran.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      
									  $id=$_POST['id_'];
									  $kd=$_POST['kd'];
									  $brg=$_POST['brg'];
                                      $jml=$_POST['jml'];
                                      $ket=$_POST['ket'];
									  $hrg=$_POST['hrg'];
                                      
								     
									  
                                      if(isset($brg,$jml)){
                                        if((!$brg)||(!$jml)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO permintaan VALUES ('','$id','$kd','$brg','$hrg','$jml','1','$ket',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);
									 
                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=permintaan.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>