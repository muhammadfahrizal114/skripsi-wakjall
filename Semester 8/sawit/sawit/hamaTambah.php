<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH AKTIFITAS HAMA</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah Hama</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl"/>
                                          </div>
										  <div class="form-group">
                                            <label>Nama Hama</label>
                                            <input type="text" class="form-control" name="hama"/>
                                          </div>
                                          <div class="form-group">
										    <label>Tingkat Kerusakan</label>
                                            <input type="text" class="form-control" name="rusak"/>
                                          </div>
                                          <div class="form-group">
										    <label>Jumlah Kerusakan</label>
                                            <input type="number" class="form-control" name="jml"/>
                                          </div>
										  <div class="form-group">
										    <label>Kebun</label>
											<select name="kebun" id="kebun" class="form-control">
												<option>--Pilih Kebun--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM kebun");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_kebun']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
                                            <label>Blok</label>
											<select name="blok" id="blok" class="form-control">
												<option>--Pilih Blok--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM blok");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_blok']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
										    <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" cols="10" rows="8"></textarea>
                                          </div>
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="kriteria.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $tgl=$_POST['tgl'];
                                      $hama=$_POST['hama'];
                                      $rusak=$_POST['rusak'];
									  $jml=$_POST['jml'];
									  $kebun=$_POST['kebun'];
									  $blok=$_POST['blok'];
									  $ket=$_POST['ket'];
                                      

                                      if(isset($tgl,$hama)){
                                        if((!$tgl)||(!$hama)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO hama VALUES ('','$tgl','$hama','$rusak','$jml','$kebun','$blok','$ket',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=hama.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
</script>
<script type="text/javascript">
	var htmlobjek;
	$(document).ready(function(){
		//apabila terjadi onchange pada poli id
		$("#kebun").change(function(){
			var poli = $("#kebun").val();
			$.ajax({
				url:"ambilBlok.php",
				data:"blok="+poli,
				cache:false,
				success:function(msg){
					$("#blok").html(msg);
				}
			});
		});
	});
</script>

<?php
  include "footer.php";
?>