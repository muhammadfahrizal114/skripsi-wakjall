-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2022 at 12:30 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kolam`
--

-- --------------------------------------------------------

--
-- Table structure for table `analisa`
--

CREATE TABLE `analisa` (
  `id_analisa` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `id_pemilik` int(11) NOT NULL,
  `nilainya` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `analisa`
--

INSERT INTO `analisa` (`id_analisa`, `id_kriteria`, `id_pemilik`, `nilainya`) VALUES
(1, 1, 1, '3'),
(2, 2, 1, '2'),
(3, 3, 1, '3'),
(4, 4, 1, '2'),
(5, 5, 1, '2'),
(6, 6, 1, '2'),
(7, 1, 2, '5'),
(8, 2, 2, '4'),
(9, 3, 2, '5'),
(10, 4, 2, '4'),
(11, 5, 2, '4'),
(12, 6, 2, '4'),
(13, 1, 3, '5'),
(14, 2, 3, '2'),
(15, 3, 3, '3'),
(16, 4, 3, '2'),
(17, 5, 3, '4'),
(18, 6, 3, '3'),
(19, 1, 4, '3'),
(20, 2, 4, '2'),
(21, 3, 4, '3'),
(22, 4, 4, '2'),
(23, 5, 4, '2'),
(24, 6, 4, '4'),
(26, 1, 5, '5'),
(27, 2, 5, '2'),
(28, 3, 5, '3'),
(29, 4, 5, '3'),
(30, 5, 5, '3'),
(31, 6, 5, '4'),
(32, 1, 8, '5'),
(33, 2, 8, '2'),
(34, 3, 8, '4'),
(35, 4, 8, '2'),
(36, 5, 8, '2'),
(37, 6, 8, '4'),
(38, 1, 9, '4'),
(39, 2, 9, '2'),
(40, 3, 9, '2'),
(41, 4, 9, '2'),
(42, 5, 9, '2'),
(43, 6, 9, '3'),
(44, 1, 10, '4'),
(45, 2, 10, '2'),
(46, 3, 10, '2'),
(47, 4, 10, '2'),
(48, 5, 10, '2'),
(49, 6, 10, '2'),
(50, 1, 11, '5'),
(51, 2, 11, '3'),
(52, 3, 11, '2'),
(53, 4, 11, '3'),
(54, 5, 11, '2'),
(55, 6, 11, '2'),
(56, 1, 12, '5'),
(57, 2, 12, '2'),
(58, 3, 12, '3'),
(59, 4, 12, '4'),
(60, 5, 12, '2'),
(61, 6, 12, '2'),
(63, 1, 13, '5'),
(64, 2, 13, '2'),
(65, 3, 13, '3'),
(66, 4, 13, '5'),
(67, 5, 13, '2'),
(68, 6, 13, '2'),
(69, 1, 14, '3'),
(70, 2, 14, '2'),
(71, 3, 14, '2'),
(72, 4, 14, '2'),
(73, 5, 14, '2'),
(74, 6, 14, '2');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `nama_fakultas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `nama_fakultas`) VALUES
(1, 'Teknik Komunikasi Dan Elektro'),
(2, 'Teknologi Industri Dan Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id_kamar` int(11) NOT NULL,
  `id_pemilik` int(11) NOT NULL,
  `parkir` int(11) NOT NULL,
  `toilet` int(11) NOT NULL,
  `kantin` int(11) NOT NULL,
  `perosotan` int(11) NOT NULL,
  `waterboom` int(11) NOT NULL,
  `musholla` int(11) NOT NULL,
  `kolamMandiBusa` int(11) NOT NULL,
  `kolamArus` int(11) NOT NULL,
  `kolamSlideSpiral` int(11) NOT NULL,
  `gazebo` int(11) NOT NULL,
  `motorATV` int(11) NOT NULL,
  `bomerang` int(11) NOT NULL,
  `scopusslider` int(11) NOT NULL,
  `olimpicPool` int(11) NOT NULL,
  `kolamOmbak` int(11) NOT NULL,
  `flyingFox` int(11) NOT NULL,
  `crocodilePool` int(11) NOT NULL,
  `loker` int(11) NOT NULL,
  `kolamPancing` int(11) NOT NULL,
  `paintBall` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `id_pemilik`, `parkir`, `toilet`, `kantin`, `perosotan`, `waterboom`, `musholla`, `kolamMandiBusa`, `kolamArus`, `kolamSlideSpiral`, `gazebo`, `motorATV`, `bomerang`, `scopusslider`, `olimpicPool`, `kolamOmbak`, `flyingFox`, `crocodilePool`, `loker`, `kolamPancing`, `paintBall`) VALUES
(1, 2, 0, 0, 500000, 0, 20180502, 20180502, 20180502, 20180502, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 0, 0, 500000, 0, 20180502, 20180502, 20180502, 20180502, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 0, 0, 350000, 0, 20180502, 20180502, 20180502, 20180502, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 4, 0, 0, 450000, 0, 20180502, 20180502, 20180502, 20180502, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 5, 0, 0, 400000, 0, 20180502, 20180502, 20180502, 20180502, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 8, 0, 0, 1000000, 0, 2147483647, 2147483647, 2147483647, 2147483647, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 9, 0, 0, 750000, 0, 2147483647, 2147483647, 2147483647, 2147483647, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 10, 0, 0, 400000, 0, 2147483647, 2147483647, 2147483647, 2147483647, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 11, 0, 0, 300000, 0, 2147483647, 2147483647, 2147483647, 2147483647, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 12, 0, 0, 800000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 13, 0, 0, 450000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 14, 0, 0, 500000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `atribut` varchar(50) NOT NULL,
  `bobot_nilai` varchar(50) NOT NULL,
  `nama_kriteria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `atribut`, `bobot_nilai`, `nama_kriteria`) VALUES
(1, 'benefit', '3', 'Fasilitas'),
(2, 'cost', '2', 'Biaya'),
(5, 'cost', '3', 'Jarak'),
(6, 'benefit', '2', 'Jumlah Kolam');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `user` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `alamat` text COLLATE latin1_general_ci NOT NULL,
  `no_telepon` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `password_asli` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `status` enum('admin','pemilik','user') COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user`, `nama`, `alamat`, `no_telepon`, `email`, `password`, `password_asli`, `status`) VALUES
('admin', 'administrator', '', '', '', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin'),
('avit', 'avit', 'purwokerto', '085726345645', 'avita.rinovita@gmail', '21232f297a57a5a743894a0e4a801fc3', '', 'user'),
('budi', 'budi', 'jalan selatan kampus telkom purwokerto ', '085767657647', 'budi@gmail.com', 'eea2c1e5e921bba51478fb8ff99fa077', '', 'user'),
('anton', 'anton', 'jalan arsadimeja no 30 rt 2/4 purwokerto banyumas', '08156972222', 'pihcynkmih@rocketmail.com', '75f9b0ef29785f83d7ebfc04c3bdb1f9', '', 'user'),
('dita', 'Dita Agustika', 'Jalan Prof Dr Soeharso Kavling Gelora Indah 1 Gang Bolling No.1', '082313359574', 'dita@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '', 'pemilik');

-- --------------------------------------------------------

--
-- Table structure for table `pemilik`
--

CREATE TABLE `pemilik` (
  `id_pemilik` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jamOperasional` varchar(50) NOT NULL,
  `kolamDewasa` varchar(15) NOT NULL,
  `kolamAnak` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `hargaMasuk` varchar(10) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemilik`
--

INSERT INTO `pemilik` (`id_pemilik`, `user`, `nama`, `jamOperasional`, `kolamDewasa`, `kolamAnak`, `alamat`, `hargaMasuk`, `latitude`, `longitude`) VALUES
(1, '', 'GM POOL\'S', 'GM POOL\'S', '085291162992', 'Jl. DI Panjaitan Gang Karang Baru II No 63 Purwoke', '', 'y', '', ''),
(2, '', 'PARADUTA SWIMMING POOL ', 'PARADUTA SWIMMING POOL', '08156972222', 'Jl DI Panjaitan Gg Karang Baru III No 199', '', 'y', '', ''),
(3, '', 'BORNEO WATERPARK ', 'BORNEO WATERPARK ', '081327085692', 'Jl DI Panjaitan', '', 'y', '', ''),
(4, '', 'KOLAM  RENANG ZAVFASZA ', 'KOLAM  RENANG ZAVFASZA', '081327085692', 'Jl. D.I. Panjaitan, Purwokerto Kidul, Purwokerto S', '', 'y', '', ''),
(5, '', 'KOLAM RENANG AULIA ', 'KOLAM RENANG AULIA ', '08242759755', 'Jl Di Panjaitan', '', 'y', '', ''),
(8, '', 'LABERSA WATER AND THEME PARK ', 'LABERSA WATER AND THEME PARK', '085726847653', 'Perumahan Berkoh Indah Blok G III No. 328, Purwoke', '', 'y', '', ''),
(9, '', 'BOOMBARA WATERPARK ', 'BOOMBARA WATERPARK', '08164880053', 'jalan Sunan Kalijaga gang VI no 6A, Berkoh - Purwo', '', 'y', '', ''),
(10, '', 'CITRALAND WATERPARK PEKANBARU ', 'CITRALAND WATERPARK PEKANBARU', '085726063530', 'jl.puteran, berkoh purwokerto', '', 'y', '', ''),
(11, '', 'KOLAM RENANG NILA PEKANBARU ', 'KOLAM RENANG NILA PEKANBARU', '083845046050', 'Jl. Ahmad Djaelani Karangwangkal Purwokerto Utara', '', 'y', '', ''),
(12, '', 'KOLAM RENANG PELANGI DARMA ', 'KOLAM RENANG PELANGI DARMA', '028161392418', 'Jl. Sunan Kalijaga Gg VI No. 36 RT 04/ RW 02 Berko', '', 'y', '', ''),
(13, '', 'DNA FUN ZONE SWIMMING POOL ', 'DNA FUN ZONE SWIMMING POOL', '081390519455', 'perumahan ketapang indah blok D1 no 62 purwokerto ', '', 'y', '', ''),
(14, '', 'KOLAM RENANG ARAS ', 'KOLAM RENANG ARAS ', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(18, 'dita', 'SENTOSA SWIMMING POOL ', 'SENTOSA SWIMMING POOL', '085676545444', 'jalan perintis kemerdekaan no 15 ', '', '', '', ''),
(19, '', 'KOLAM TELAGA BIRU ', 'KOLAM TELAGA BIRU ', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(20, '', 'KOLAM BANGAU SAKTI ', 'KOLAM BANGAU SAKTI', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(21, '', 'CITRA SARI KOLAM RENANG ', 'CITRA SARI KOLAM RENANG', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(22, '', 'DEFA SWIMMING POOL ', 'DEFA SWIMMING POOL ', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(23, '', 'KOLAM RENANG ANGKASA ', 'KOLAM RENANG ANGKASA', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(24, '', 'KOLAM RENANG REFENA ', 'KOLAM RENANG REFENA ', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', ''),
(25, '', 'KOLAM RENANG MELUR ', ' 	KOLAM RENANG MELUR', '081542624186', 'Komplek Gor Satria Purwokerto', '', 'y', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `progdi`
--

CREATE TABLE `progdi` (
  `id_progdi` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `nama_progdi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progdi`
--

INSERT INTO `progdi` (`id_progdi`, `id_fakultas`, `nama_progdi`) VALUES
(1, 1, 'D3 Teknik Telekomunikasi'),
(2, 1, 'S1 Teknik Telekomunikasi '),
(3, 1, 'S1 Teknik Elektro'),
(4, 2, 'S1 Teknik Informatika'),
(5, 2, 'S1 Software Engineering'),
(6, 2, 'S1 Sistem Informasi'),
(7, 2, 'S1 Teknik Industri'),
(8, 2, 'S1 Desain Komunikasi Visual');

-- --------------------------------------------------------

--
-- Table structure for table `t_kriteria`
--

CREATE TABLE `t_kriteria` (
  `id_tkriteria` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `nkriteria` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kriteria`
--

INSERT INTO `t_kriteria` (`id_tkriteria`, `id_kriteria`, `keterangan`, `nkriteria`) VALUES
(1, 1, 'Kasur,Alamari', '1'),
(2, 1, 'kasur, alamari,meja', '2'),
(3, 1, 'kasur,almari,meja,kipas angin', '3'),
(4, 1, 'kasur,almari,meja,kursi,kipas angin', '4'),
(5, 1, 'kasur,alamari,meja,kursi,kipas angin,tv', '5'),
(6, 2, '>=500000', '1'),
(7, 2, '>500000<300000', '2'),
(8, 2, '>300000<250000', '3'),
(9, 2, '>250000<200000', '4'),
(10, 2, '<=200000', '5'),
(11, 3, 'dekat kampus,ruangan kamar luas', '1'),
(12, 3, 'dekat kampus,dekat warung makan', '2'),
(13, 3, 'dekat kampus,dekat warung makan,alfamart dan pasar', '3'),
(14, 3, 'dekat dekat kampus,dekat warung makan,alfamart dan mall', '4'),
(15, 3, 'dekat kampus,dekat warung makan,alfamart,pasar,atm,mall', '5'),
(16, 4, 'keamanan dari cctv', '1'),
(17, 4, 'penjaga kos', '2'),
(18, 4, 'tinggal bersama pemilik kos', '3'),
(19, 4, 'bersama pemilik kos dan terdapat cctv', '4'),
(20, 4, 'tanggung jawab bersama', '5'),
(21, 5, '>=1KM', '1'),
(22, 5, '>1KM<500M', '2'),
(23, 5, '>500M<250M', '3'),
(24, 5, '>250m<50M', '4'),
(25, 5, '<=50M', '5'),
(26, 6, 'terdapat jadwal piket anak kos', '1'),
(27, 6, 'kebersihan diambil dari pihak luar', '2'),
(28, 6, 'pemilik kos membantu kebersihan hunian kos', '3'),
(29, 6, 'kebersihan menjadi tanggung jawab bersama', '4'),
(30, 6, 'kebersihan rutin menjadi tanggung jawab pemilik kos', '5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analisa`
--
ALTER TABLE `analisa`
  ADD PRIMARY KEY (`id_analisa`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`id_pemilik`);

--
-- Indexes for table `progdi`
--
ALTER TABLE `progdi`
  ADD PRIMARY KEY (`id_progdi`);

--
-- Indexes for table `t_kriteria`
--
ALTER TABLE `t_kriteria`
  ADD PRIMARY KEY (`id_tkriteria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analisa`
--
ALTER TABLE `analisa`
  MODIFY `id_analisa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pemilik`
--
ALTER TABLE `pemilik`
  MODIFY `id_pemilik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `progdi`
--
ALTER TABLE `progdi`
  MODIFY `id_progdi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_kriteria`
--
ALTER TABLE `t_kriteria`
  MODIFY `id_tkriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
