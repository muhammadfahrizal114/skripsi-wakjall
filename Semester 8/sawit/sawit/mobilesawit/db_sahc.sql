-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2022 at 11:11 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sahc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL,
  `namaAdmin` varchar(35) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idAdmin`, `namaAdmin`, `username`, `password`) VALUES
(1, 'Administrator', 'admin', '$2y$10$zMq/i1JYiXN8bPTxwReWLeEyZld445XtisWTf3Rt3jNul2l6NecFm');

-- --------------------------------------------------------

--
-- Table structure for table `jalur`
--

CREATE TABLE `jalur` (
  `idJalur` int(11) NOT NULL,
  `awal` int(11) NOT NULL,
  `tujuan` int(11) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `kdKegiatan` int(11) NOT NULL,
  `idPanti` int(11) NOT NULL,
  `namaKegiatan` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `panti`
--

CREATE TABLE `panti` (
  `idPanti` int(11) NOT NULL,
  `noIzin` text NOT NULL,
  `namaPanti` varchar(50) NOT NULL,
  `alamatPanti` text NOT NULL,
  `telpPanti` varchar(18) NOT NULL,
  `statusHukumPanti` enum('Berbadan Hukum','Tidak Berbadan Hukum') NOT NULL,
  `idPengurus` int(11) NOT NULL,
  `statusPanti` enum('Aktif','Tidak Aktif') NOT NULL,
  `latitude` varchar(10) NOT NULL,
  `longitude` varchar(10) NOT NULL,
  `jarak` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `panti`
--

INSERT INTO `panti` (`idPanti`, `noIzin`, `namaPanti`, `alamatPanti`, `telpPanti`, `statusHukumPanti`, `idPengurus`, `statusPanti`, `latitude`, `longitude`, `jarak`) VALUES
(1, '9/05.50/DPMPTSP/VIII/2020', 'PA. AL- MUZAKKI', 'Jl. Melur Gg. Buntu III Kel. Sidomulyo Barat Kec. Tampan Kota Pekanbaru', '081365534178', 'Berbadan Hukum', 1, 'Aktif', '0.453344', '101.412198', 1.05),
(2, '7/05.50/DPMPTSP/III/2021', 'PA. AL- AKBAR', 'Jl. Kaharuddin Nasution NO. 66 Maharatu, Marpoyan Damai Pekanbaru', '081365332924', 'Berbadan Hukum', 2, 'Aktif', '0.457189', '101.452367', 3.83),
(3, '6/05.50/DPMPTSP/VIII/2020', 'PA. AN - NISA', 'Jl. Dakota No. 38, Tengkerang Tengah, Marpoyan Damai Pekanbaru', '085264106022', 'Berbadan Hukum', 3, 'Aktif', '0.494576', '101.442060', 4.4),
(4, '479/411.42/PEMSOS/2015', 'PA. AL - ANSOR', 'Jl. Singgalang Raya No. 313 Tangkerang Timur', '076125829', 'Berbadan Hukum', 4, 'Aktif', '0.490535', '101.503325', 7.85),
(5, '12/05.50/DPMPTSP/V/2021', 'PA. INSAN PERMATA', 'Jl. Fajar 3 No. 6 Labuh Baru Barat', '085211315538', 'Berbadan Hukum', 5, 'Aktif', '0.530136', '101.329120', 7.33);

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `idPengurus` int(11) NOT NULL,
  `namaPengurus` varchar(35) NOT NULL,
  `periodePengurus` varchar(35) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`idPengurus`, `namaPengurus`, `periodePengurus`, `username`, `password`) VALUES
(1, 'Ardianto', '2020-2025', 'ardianto', '123'),
(2, 'Yuli Marni, S.I.Kom', '2020-2025', 'yuli', '123'),
(3, 'Tri Rahayu', '2020-2025', 'tri', '123'),
(4, 'Wahyudi Samsul Ridwan', '2018-2020', 'wahyudi', '123'),
(5, 'Adriana Sandra Linda Keles', '2020-2025', 'adriana', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Indexes for table `jalur`
--
ALTER TABLE `jalur`
  ADD PRIMARY KEY (`idJalur`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`kdKegiatan`);

--
-- Indexes for table `panti`
--
ALTER TABLE `panti`
  ADD PRIMARY KEY (`idPanti`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`idPengurus`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jalur`
--
ALTER TABLE `jalur`
  MODIFY `idJalur` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `kdKegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `panti`
--
ALTER TABLE `panti`
  MODIFY `idPanti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `idPengurus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
