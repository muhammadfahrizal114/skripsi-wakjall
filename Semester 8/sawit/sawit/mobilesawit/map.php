<!DOCTYPE html>

<html>
<head>
	<style>
		html,
		body,
		#map {
		  height: 100%;
		  width: 100%;
		  margin: 0;
		  padding: 0;
		}
	</style>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <!-- <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyD34NfB-_v1C5bqBCBLukmjae9yG2YO5D8"></script>-->
	<script async='false' defer src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyCU61c8oBdfuzO2kVpTvdXwMYbzCDHmeeY"></script>
    <script src="jQuery-2.2.0.min.js"></script>
</head>

<body>
<div id="output"></div>
<div id="map"></div>
<script>
  var map;
  function initMap() {
    var directionsService = new google.maps.DirectionsService;
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 0.4491031824857245, lng: 101.39765084076305},
      zoom: 8
    });
	
  var polylineOptionsActual = new google.maps.Polyline({
    strokeColor: 'Green',
    strokeOpacity: 1.0,
    strokeWeight: 4
    });
    
 
  var listPos = [
     
     {arriveeLat: 0.450590111865072, arriveeLng: 101.3911737145102,
      departLat: 0.4380592084229772, departLng: 101.39173161396414},
      
     {arriveeLat:0.4380592084229772, arriveeLng: 101.391731613964146,
      departLat: 0.4437238660347219, departLng: 101.39984261371782},
      
	 {arriveeLat: 0.4437238660347219, arriveeLng: 101.39984261371782,
      departLat: 0.4420502176438172, departLng: 101.403640621539},
    
     ];
  var bounds = new google.maps.LatLngBounds();
  for (var i = 0; i < listPos.length; i++) {
	var startPoint = new google.maps.LatLng(listPos[i]['departLat'], listPos[i]['departLng']);
    var endPoint = new google.maps.LatLng(listPos[i]['arriveeLat'], listPos[i]['arriveeLng']);
    var directionsDisplay = new google.maps.DirectionsRenderer({map: map, preserveViewport: true, suppressMarkers: true});
    calculateAndDisplayRoute(directionsService, directionsDisplay, startPoint, endPoint, bounds);
  }

}


  function calculateAndDisplayRoute(directionsService, directionsDisplay, startPoint, endPoint, bounds) {
    directionsService.route({
      origin: startPoint,
      destination: endPoint,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        console.log(response);
        directionsDisplay.setDirections(response);
        bounds.union(response.routes[0].bounds);
        map.fitBounds(bounds);
      } else {
        window.alert('Impossible d afficher la route ' + status);
      }
    });
  }

</script>
<br>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)">refresh</a>
	
</body>
</html>