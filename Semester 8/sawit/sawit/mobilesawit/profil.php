<!DOCTYPE HTML>
<html lang="en">
<?php
session_start();
include("connect.php");

if(isset($_POST['simpan'])){
    mysqli_query($konek,"update users set nik='$_POST[nik]',nama='$_POST[nama]',email='$_POST[email]',telepon='$_POST[telp]',alamat='$_POST[alamat]' where id='$_SESSION[idUser]'");
}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>StickyMobile BootStrap</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

<div id="page">
<div class="header header-fixed header-logo-center">
<a href="index.html" class="header-title">PROFIL</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php"><i class="fa fa-home"></i><span>Home</span></a>
<a href="menuUtama.php"><i class="fas fa-th"></i><span>Menu</span></a>
<a href="profil.php" data-menu="menu-settings" class="active-nav"><i class="fa fa-user" style="color:blue !important;"></i><span style="color:blue !important;">Profil</span></a>
</div>
<br>
<?php
echo $_SESSION['idUser'];
$result = mysqli_query($konek,"select * from users where id='$_SESSION[idUser]'");
$row = mysqli_fetch_array($result); 
?>
<div class="page-content header-clear-medium">
<div class="card card-style" data-card-height="100">
<div class="card-center p-3">
<br>
<h1 class="color-white"><?php echo $row['nama']; ?></h1>
<p class="color-white opacity-50"><?php echo $row['email']; ?></p>
</div>
<div class="card-top p-3">
<span class="badge bg-green-dark color-white p-2 float-end"><?php echo $row['status']; ?></span>
</div>

<div class="card-overlay bg-black opacity-70"></div>
</div>
<div class="content">
<div class="row">
<div class="col-12">
<a href="#" class="btn btn-full btn-m bg-green-dark rounded-sm bg-highlight text-uppercase font-800" data-menu="menu-login-1"></i>Edit Profil</a>
</div>
</div>
</div>
<div class="card card-style">
<div class="content">
<div class="d-flex">
<div class="align-self-center">
<h2 class="mb-0">Data Profil</h2>
</div>
<div class="align-self-center ms-auto">
</div>
</div>
<div class="divider mt-3 mb-3"></div>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-play-circle pe-2 "></i><?php echo $row['nik']; ?></a>
</div>

</div>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i><?php echo $row['nama']; ?></a>
</div>
</div>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-play-circle pe-2 "></i><?php echo $row['email']; ?></a>
</div>

</div>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-play-circle pe-2 "></i><?php echo $row['telepon']; ?></a>
</div>

</div>
</div>
</div>
<div class="content">
<div class="row">
<div class="col-12">
<a href="keluar.php" class="btn btn-full btn-m bg-red-dark rounded-sm bg-highlight text-uppercase font-800"></i>Keluar</a>
</div>
</div>
</div> 
</div>

</div>
<div id="menu-login-1" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="500" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Lengkapi Data</h1>
<p class="color-theme opacity-50">Isi form dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form method="post" action="profil.php" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon validate-field mb-4">
<input type="hidden" class="form-control validate-name" id="form1a" name='id' value="<?php echo $row['id']; ?>">
<input type="text" class="form-control validate-name" id="form1a" name='nik' value="<?php echo $row['nik']; ?>">
<label for="form1a" class="color-blue-dark">No. NIK</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<input type="text" class="form-control validate-text" id="form3a" name='email' value="<?php echo $row['email']; ?>">
<label for="form3a" class="color-blue-dark">Email</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<input type="text" class="form-control validate-text" id="form3a" name='nama' value="<?php echo $row['nama']; ?>">
<label for="form3a" class="color-blue-dark">Nama</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<input type="text" class="form-control validate-text" id="form3a" name='telp' value="<?php echo $row['telepon']; ?>">
<label for="form3a" class="color-blue-dark">No. Telp</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<input type="text" class="form-control validate-text" id="form3a" name='alamat' value="<?php echo $row['alamat']; ?>">
<label for="form3a" class="color-blue-dark">Alamat</label>
</div>

<button type="submit" name="simpan" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-green-dark mt-n2" style="width:100%">Update Profil</button>
</form>
</div>
</div>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
</body>