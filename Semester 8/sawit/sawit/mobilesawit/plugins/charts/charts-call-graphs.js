var lineChart = document.getElementById('line-chart')

var lineDemoChart = new Chart(lineChart, {
    type: 'line',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6 , 7 , 8, 9, 10, 11, 12, 13],
        datasets: [{
                data: [0, 1, 5, 6 , 7 , 8, 9, 10, 11, 12, 13],
                label: "Berat Badan",
                borderColor: "#D8334A"
  }
]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                fontSize: 13,
                padding: 15,
                boxWidth: 12
            },
        },
        title: {
            display: false
        }
    }
});
