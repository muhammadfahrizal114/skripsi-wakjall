<!DOCTYPE HTML>
<?php
include("connect.php");
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>Topsis</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
<div class="header header-fixed header-logo-center">
<a href="index.html" class="header-title">INFO DETAIL WISATA</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php" ><i class="fa fa-home" ></i><span >Home</span></a>
<a href="cariRute.php" class="active-nav"><i class="fas fa-file" style="color:blue !important;"></i><span style="color:blue !important;">Wisata</span></a>
<a href="cariRute.php"><i class="fas fa-map-marker"></i><span>Rute</span></a>
<a href="" data-menu="menu-login-1"><i class="fa fa-user"></i><span>Profil</span>
</a>
</div>
<div class="page-content header-clear-medium">

<div class="row mb-0">
<div class="col-6 pe-0" style="width:97.5%">
<div class="card card-style">
<div class="content mb-3">
<center><a href="" onClick="document.location.reload(true)"><img src="images/logo/6.jpg" width="330px" height="250px" /></a></center><br>
<table width="100%">
<tr>
<td>
<h3>ISTANA SIAK</h3>
<p>Alamat :  Kabupaten Siak<br>
Deskripsi: <br>
Istana Siak Sri Inderapura (Jawi: ايستان سياق سري ايندراڤورا) atau Istana Asserayah Hasyimiah atau Istana Matahari Timur merupakan kediaman resmi Sultan Siak yang mulai dibangun pada tahun 1889, 
yaitu pada masa pemerintahan Sultan Syarif Hasyim. Istana ini merupakan peninggalan Kesultanan Siak Sri 
Inderapura yang selesai dibangun pada tahun 1893. Kini istana ini masuk wilayah administrasi pemerintahan Kabupaten Siak.<br><br>
Kategori : Wisata Budaya
</p>
</td>
</tr>
<tr>
<td align="right" valign="bottom">
<br>
<a href="" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:100%x">Info Lokasi</button>
</td>
</tr>
</table>
</div>
</div>
</div>
</div>


</div>
</div>

</div>

<div id="menu-login-1" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="350" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Cari Kolam Renang</h1>
<p class="color-theme opacity-50">Pilih kriteria dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form method="post" action="cari.php" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon mb-4">
<select class="form-control" name="harga">
<option value="">..:: Pilih Harga Tiket ::..</option>
<option value="A1"> < Rp. 10.000 </option>
<option value="A2"> Rp. 11.000 - 30.000 </option>
<option value="A3"> Rp. 31.000 - 40.000 </option>
<option value="A4"> Rp. 41.000 - 50.000 </option>
<option value="A5"> > Rp. 50.000 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="kolam">
<option value="">..:: Pilih Jumlah Kolam ::..</option>
<option value="B1"> 1 - 2 </option>
<option value="B2"> 3 - 4 </option>
<option value="B3"> 5 - 6 </option>
<option value="B4"> 7 - 8 </option>
<option value="B5"> 9 - 10 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="fasilitas">
<option value="">..:: Pilih Jumlah Fasilitas ::..</option>
<option value="C1"> 1 - 4 </option>
<option value="C2"> 5 - 8 </option>
<option value="C3"> 9 - 12 </option>
<option value="C4"> 13 - 16 </option>
<option value="C5"> 17 - 20 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="jarak">
<option value="">..:: Pilih Jarak (Km)::..</option>
<option value="D1"> 1 - 4 </option>
<option value="D2"> 5 - 8 </option>
<option value="D3"> 9 - 11 </option>
<option value="D4"> 12 - 15 </option>
<option value="D5"> > 15 </option>
</select>
</div>
<div class="row">
<div class="col-6">
</div>
<div class="col-6">
<div class="clearfix"></div>
</div>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
</div>
<br>
<button type="submit" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:100%"><i class="fa fa-search"></i> Cari</button>
<br>
</form>
</div>
</div>

<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
<script>
$(document).ready(function() {
var x = document.getElementById("demo");
var lat = document.getElementById("lat");
var long = document.getElementById("longi");

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
 
 
 function showPosition(position) {
  lat.value = position.coords.latitude;
  long.value = position.coords.longitude;
}
});
</script>
</body>
