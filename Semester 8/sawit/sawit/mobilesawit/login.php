<!DOCTYPE HTML>
<?php
    session_start();
    include "dbconf.php";
    if (isset($_SESSION['login']))
		header('Location: utama.php');
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>SISWIT</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">

<div class="page-content header-clear-small">
<br><br>
<div class="" style="margin-top:12px;">
<div class="content mt-4 mb-0">
<center><a href="" onClick="document.location.reload(true)"><img src="images/logo/logoWisata.png" width="250px" height="250px" /></a></center><br>
<h1 class="text-center font-900 font-40 text-uppercase mb-0">Login</h1>
<p class="bottom-0 text-center color-highlight font-11">Let's get you logged in</p>
<form method="post" target="_self">
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-user"></i>
<input type="hidden" id="lat" name="lat" >
<input type="hidden" id="long" name="long">
<input type="text" class="form-control validate-name" name="user" id="form1a" placeholder="Username" style="background-color: #fff0;">
<label for="form1a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Username</label>
<em>(required)</em>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-lock"></i>
<input type="password" class="form-control validate-password" id="form3a" name="pass" placeholder="Password" style="background-color: #fff0;">
<label for="form3a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Password</label>
<em>(required)</em>
</div>
<button type="submit" class="btn btn-m mt-2 mb-4 btn-full bg-blue-dark text-uppercase font-900" style="width:100%">Login</button>
<div class="divider"></div>
<div class="divider mt-4 mb-3"></div>
</form>
</div>
</div>
<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
		if($_POST && $_POST['user']!='' && $_POST['pass']!=''){
			$result = $mysqli->query("select * from users WHERE username= '".$_POST['user']."' and password= '".md5($_POST['pass'])."'");
			if($result->num_rows != 0){
				while ($row = $result->fetch_assoc()) {
					$_SESSION['login'] = 'KJHAbkfase86234809701234hgvbKHJGVYH%$&^$%&$^*';
					$_SESSION['user'] = $row['username'];
					$_SESSION['email'] = $row['email'];
					$_SESSION['nama'] = $row['nama'];
					$_SESSION['level'] = $row['id_groupuser'];
					$_SESSION['idUser'] = $row['id'];
					
					$url = "utama.php";
					header('Location: '.$url);
		    	}
			}else { 
				header('Location: utama.php');
		}
		}
	}
?>
</div>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
<script>
$(document).ready(function() {
var x = document.getElementById("demo");
var lat = document.getElementById("lat");
var long = document.getElementById("long");

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
 
 
 function showPosition(position) {
  lat.value = position.coords.latitude;
  long.value = position.coords.longitude;
}
});
</script>
</body>
