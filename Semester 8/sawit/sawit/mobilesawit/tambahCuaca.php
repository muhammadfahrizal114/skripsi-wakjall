<!DOCTYPE HTML>
<?php
session_start();
include("connect.php");
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>StickyMobile BootStrap</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
<div class="header header-fixed header-logo-center">
<a href="cuaca.php" class="header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
<a href="#" class="header-title" style="left:48% !important">FORM DATA CUACA</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php" ><i class="fa fa-home"></i><span >Home</span></a>
<a href="menuUtama.php" class="active-nav"><i class="fas fa-th"  style="color:blue !important;"></i><span style="color:blue !important;">Menu</span></a>
<a href="profil.php"><i class="fas fa-user"></i><span>Profile</span></a>
</div>
<div class="page-content header-clear-medium">

<div class="row mb-0">
<div class="col-6 pe-0" style="width:97.5%">
<div class="card card-style">
<div class="content mb-3">

<div class="menu-title mt-n1">
<h1>Input Data Cuaca</h1>
<p class="color-theme opacity-50">Isi form dibawah ini..</p>
</div>
<form method="post" action="prosesTambahCuaca.php" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon mb-4">
<input type="date" name="tanggal" class="form-control" />
</div>
<div class="input-style no-borders has-icon mb-4">
<input type="text" name="kond" class="form-control" placeholder="Kondisi Cuaca" />
</div>
<div class="input-style no-borders has-icon mb-4">
<input type="text" name="ket" class="form-control" placeholder="Catatan" />
</div>

<br>
<button type="submit" name="tambah" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-green-dark mt-n2" style="width:100%">Tambah</button>
<br>
</form>
</div>

</div>
</div>
</div>
</div>
</div>


<div id="menu-barluar" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="350" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Tambah Barang Keluar</h1>
<p class="color-theme opacity-50">Isi form dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form method="post" action="proses_barluar.php" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style has-borders no-icon mb-4">
<label for="form5" class="color-highlight">Barang</label>
<select id="form5" name="kode">
<option value="">..:Pilih Barang:..</option>
<?php
$sqlBarang = mysqli_query($conn,"select * from barang");
while($dta = mysqli_fetch_array($sqlBarang)){
?>
<option value="<?php echo $dta['kd_barang'] ?>"><?php echo $dta['namaBarang'] ?></option>
<?php
}
?>
</select>
<span><i class="fa fa-chevron-down"></i></span>
<em></em>
</div>
<div class="input-style has-borders no-icon validate-field mb-4">
<input type="text" class="form-control" name="barang" id="form4" placeholder="Jumlah Barang">
<label for="form4" class="color-highlight">Jumlah Barang</label>
</div>
<div class="input-style has-borders no-icon validate-field mb-4">
<input type="date" class="form-control" id="form4" name="tanggal" placeholder="Phone">
<label for="form4" class="color-highlight">Tanggal</label>
</div>


<br>
<button type="submit" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-green-dark mt-n2" style="width:100%">Tambah Barang Keluar</button>
<br>
</form>
</div>
</div>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
</body>
