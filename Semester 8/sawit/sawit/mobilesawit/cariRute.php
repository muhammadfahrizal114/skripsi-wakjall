<!DOCTYPE HTML>
<?php
session_start();
include("connect.php");

$kol = mysqli_query($konek, "update position set lat='$_GET[lat]', longi='$_GET[longi]' where id='1'");

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
}

$kol = mysqli_query($konek, "select * from panti");
while($sem = mysqli_fetch_array($kol)){
    
    $latuser = $_GET['lat'];
    $longuser = $_GET['longi'];
    $latKol = $sem['latitude'];
    $longKol = $sem['longitude'];

    $jaRil = round(distance(0.44910 ,101.397539 ,$latKol,$longKol,"KM"),2);                                        
    $updateJarak = mysqli_query($konek, "update panti set jarak='$jaRil' where idPanti='$sem[idPanti]'");
}
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>StickyMobile BootStrap</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
<div class="header header-fixed header-logo-center">
<a href="#" onclick="javascript:getLocation()" class="header-icon header-icon-1"><i class="fas fa-map-marker"></i></a>
<a href="index.html" class="header-title">Cari Rute</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php"><i class="fa fa-home"></i><span>Home</span></a>
<a href="cariRute.php" class="active-nav"><i style="color:blue !important;" class="fas fa-map-marker"></i><span style="color:blue !important;">Rute</span></a>
<a href="" data-menu="menu-login-1"><i class="fa fa-file"></i><span>Cara?</span>
</a>
</div>
<div class="page-content header-clear-medium">
<div class="row mb-0">
<?php

?>

<div class="col-6 pe-0" style="width:96.5%">
<div class="content mb-3">
<div class="d-flex" style="margin-top:10px;">
<a href="#" class="btn btn-m bg-blue-dark rounded-sm text-uppercase font-800" style="width:100%;">DAFTAR PANTI ASUHAN TERDEKAT</a>
</div>
</div>
</div>

<?php
$query = mysqli_query($konek,"select * from panti, pengurus where panti.idPengurus=pengurus.idPengurus order by jarak asc limit 2");
while($dta = mysqli_fetch_array($query)){
?>
<a href="rute.php?kd=<?php echo $dta['idPanti'] ?>"><div class="col-6 pe-0" style="width:98.5%">
<div class="card card-style">
<div class="content mb-3">
<div class="row mb-0" style="margin-bottom: -21px !important;">
<div class="col-12">
<h5 class="pb-3"><?php echo "No Izin. ".$dta['noIzin']; ?></h5>
</div>
<div class="col-12">
<h4 class="pb-3"><?php echo $dta['namaPanti']; ?></h4>
</div>
<div class="col-12">
<h5 class="pb-3"><?php echo $dta['alamatPanti']; ?></h5>
</div>
</div>
</div>
</div>
</div></a>
<?php
}
?>


</div>
<div id="menu-login-1" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="350" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Tuliskan Pengaduan Anda...</h1>
<p class="color-theme opacity-50">isilah form dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form  method="post" target="_self" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon mb-4">
<div class="color-blue-dark">Jenis Keluhan</div>
<select class="form-control" name="jenis">
<option value="">..:: Pilih Jenis Keluhan ::..</option>
<?php
$query = mysqli_query($konek,"select * from jenispengaduan");
while($dta = mysqli_fetch_array($query)){
?>
<option value="<?php echo $dta['kdJenis']; ?>"> <?php echo $dta['jenis']; ?> </option>
<?php
}
?>
</select>
</div>

<div class="input-style no-borders has-icon mb-4">
<div class="color-blue-dark">Keterangan</div>
<textarea name="keterangan" id="form1a" class="form-control" rows="5" cols="5"></textarea>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Surat Kehilangan ID Card</div>
<input type="file" name="file1" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KTP</div>
<input type="file" name="file2" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KK</div>
<input type="file" name="file3" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Slip Gaji</div>
<input type="file" name="file4" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Time Sheet</div>
<input type="file" name="file5" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KTA</div>
<input type="file" name="file6" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Ijazah Diksar</div>
<input type="file" name="file7" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Ijazah Diksar</div>
<input type="file" name="file8" class="form-control" />
</div>
<div class="row">
<div class="col-6">
</div>
<div class="col-6">
<div class="clearfix"></div>
</div>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
</div>
<br>
<button type="submit" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:100%"><i class="fa fa-search"></i> Kirim</button>
<br>
</form>
</div>
</div>


<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
<script>
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(getpos);
  } else {
  }
}

function getpos(position) {
           latx=position.coords.latitude;
           lonx=position.coords.longitude;
         // Show Lat and Lon
		   window.location.replace("cariRute.php?lat="+latx+"&longi="+lonx);
         // or send them to use elsewhere
         //  location.href = '(your file name).php?latx='+latx+'&lonx='+lonx+'&shead=<? echo $shead ?>';
}
</script>
</body>
