var bpjs ="<?php echo $_GET['bpjs']; ?>";
  $.ajax({
    url : "http://localhost/warga/grafik_get.php?bpjs="+ bpjs,
    type : "GET",
    success : function(data){
      console.log(data);

      var bb = [];

      for(var i in data) {
        bb.push(data[i].bb);
      }
	
	 var chartdata = {
        labels: [0, 1, 2, 3, 4, 5, 6 , 7 , 8, 9, 10, 11, 12, 13],
        datasets: [
          {
			data: bb,
            label: "Berat Badan",
            borderColor: "#D8334A"
          }
        ]
      };

    var ctx = $("#line-chart");

      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata,
		
		options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                fontSize: 13,
                padding: 15,
                boxWidth: 12
            },
        },
        title: {
            display: false
        }
		}
      });
    },
	
  });