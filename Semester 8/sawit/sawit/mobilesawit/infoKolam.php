<!DOCTYPE HTML>
<?php
include("../connect.php");
include "../function.php";
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>Topsis</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
<div class="header header-fixed header-logo-center">
<a href="index.html" class="header-title">INFO KOLAM</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php"><i class="fa fa-home"></i><span>Home</span></a>
<a href="rank.php" class="active-nav"><i class="fa fa-star" style="color:blue !important;"></i><span style="color:blue !important;">Top Rank</span></a>
<a href="#" data-menu="menu-login-1"><i class="fa fa-search"></i><span>Cari</span>
</a>
<a href="tentang.php" data-menu="menu-settings"><i class="fa fa-user"></i><span>Tentang</span></a>
</div>
<div class="page-content header-clear-medium">
<?php 
$query = mysqli_query($konek,"select * from kolam where id_kolam='$_GET[id]'");
$dta = mysqli_fetch_array($query);
?>
<div class="row mb-0">
<div class="col-6 pe-0" style="width:97.5%">
<div class="card card-style">
<div class="content mb-3">
<table width="100%">
<tr>
<td>
<h5><?php echo $dta['nama'] ?></h5>
<p>Jam Operasional : <?php echo $dta['jamOperasional'] ?> WIB<br>
Harga : <?php echo $dta['hargaMasuk'] ?><br>
Jarak : <?php echo $dta['jarak'] ?><br>
Kolam Dewasa : <?php echo $dta['kolamDewasa'] ?><br>
Kolam Anak : <?php echo $dta['kolamAnak'] ?>
</p>
</td>
<td align="right" valign="bottom">
<a href="map.php?id=<?php echo $dta['id_kolam']; ?>" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:150px">Info Lokasi</button>
</td>
</tr>
</table>
</div>
</div>
<?php 
$query = mysqli_query($konek,"select * from fasilitas where id_kolam='$_GET[id]'");
$dta = mysqli_fetch_array($query);
?>
<div class="card card-style">
<div class="content">
<div class="d-flex">
<div class="align-self-center">
<h2 class="mb-0">Fasilitas</h2>
</div>
<div class="align-self-center ms-auto">
</div>
</div>
<div class="divider mt-3 mb-3"></div>
<?php
if($dta['parkir'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Parkir</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Parkir</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['toilet'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Toilet</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Toilet</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['kantin'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Kantin</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Kantin</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['perosotan'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Perosotan</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Perosotan</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['waterboom'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Waterboom</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Waterboom</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['musholla'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Musholla</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Musholla</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['kolamMandiBusa'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Kolam Mandi Busa</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Kolam Mandi Busa</a>
</div>
</div>
<?php
}
?>	

<?php
if($dta['kolamArus'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Kolam Arus</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Kolam Arus</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['kolamSlideSpiral'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Kolam Slide Spiral</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Kolam Slide Spiral</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['gazebo'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Gazebo</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Gazebo</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['motorATV'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Motor ATV</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-blue font-14"><i class=""></i>Motor ATV</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['bomerang'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Boomerang</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Boomerang</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['scopusslider'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Scopus Slider</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Scopus Slider</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['olimpicPool'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Olimpic Pool</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Olimpic Pool</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['flyingFox'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Flying Fox</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Flying Fox</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['crocodilePool'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Crocodile Pool</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Crocodile Pool</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['loker'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Loker</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Loker</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['kolamPancing'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Kolam Pancing</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Kolam Pancing</a>
</div>
</div>
<?php
}
?>

<?php
if($dta['paintBall'] == 1){
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-black font-14"><i class="fa fa-check-circle pe-2 "></i>Paint Ball</a>
</div>
</div>
<?php
}else{
?>
<div class="d-flex mb-3">
<div class="align-self-center">
<a href="#" class="mb-0 color-red font-14"><i class=""></i>Pain Ball</a>
</div>
</div>
<?php
}
?>


</div>
</div>
</div>
</div>

</div>

<div id="menu-login-1" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="350" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Cari Kolam Renang</h1>
<p class="color-theme opacity-50">Pilih kriteria dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form method="post" action="cari.php" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon mb-4">
<select class="form-control" name="harga">
<option value="">..:: Pilih Harga Tiket ::..</option>
<option value="A1"> < Rp. 10.000 </option>
<option value="A2"> Rp. 11.000 - 30.000 </option>
<option value="A3"> Rp. 31.000 - 40.000 </option>
<option value="A4"> Rp. 41.000 - 50.000 </option>
<option value="A5"> > Rp. 50.000 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="kolam">
<option value="">..:: Pilih Jumlah Kolam ::..</option>
<option value="B1"> 1 - 2 </option>
<option value="B2"> 3 - 4 </option>
<option value="B3"> 5 - 6 </option>
<option value="B4"> 7 - 8 </option>
<option value="B5"> 9 - 10 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="fasilitas">
<option value="">..:: Pilih Jumlah Fasilitas ::..</option>
<option value="C1"> 1 - 4 </option>
<option value="C2"> 5 - 8 </option>
<option value="C3"> 9 - 12 </option>
<option value="C4"> 13 - 16 </option>
<option value="C5"> 17 - 20 </option>
</select>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<select class="form-control" name="jarak">
<option value="">..:: Pilih Jarak (Km)::..</option>
<option value="D1"> 1 - 4 </option>
<option value="D2"> 5 - 8 </option>
<option value="D3"> 9 - 11 </option>
<option value="D4"> 12 - 15 </option>
<option value="D5"> > 15 </option>
</select>
</div>
<div class="row">
<div class="col-6">
</div>
<div class="col-6">
<div class="clearfix"></div>
</div>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
</div>
<br>
<button type="submit" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:100%"><i class="fa fa-search"></i> Cari</button>
<br>
</form>
</div>
</div>

<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
<script>
$(document).ready(function() {
var x = document.getElementById("demo");
var lat = document.getElementById("lat");
var long = document.getElementById("longi");

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
 
 
 function showPosition(position) {
  lat.value = position.coords.latitude;
  long.value = position.coords.longitude;
}
});
</script>
</body>
