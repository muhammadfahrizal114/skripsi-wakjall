<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Pengeluaran</li>
                        </ol>
                    </div>
                   
                </div>
                
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="pengeluaranTambah.php">Tambah Pengeluaran</a></p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM pengeluaran ORDER BY tanggal DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['tanggal'];?></td>
                                            <td><?php echo $row['keterangan'];?></td>
                                            <td><?php echo $row['jumlah'];?></td>
                                        <?php
											
											 
											   print("
											    <td>
												<a class='btn btn-primary' href='upload/$row[nota]'>
                                                Lihat Nota
                                                </a>
												<a class='btn btn-warning' href=pengeluaranEdit.php?id_=$row[id]>
                                                Ubah
                                                </a>
                                                <a class='btn btn-danger' href='pengeluaranDelete.php?id_=$row[id]'>
                                                Hapus
                                                </a>
                                                </td>
                                              </tr>");
											                                                

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>