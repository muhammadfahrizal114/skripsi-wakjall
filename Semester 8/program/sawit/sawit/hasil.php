<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Hasil Panen</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
								
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kebun</th>
                                            <th>Bagus</th>
                                            <th>Kurang Bagus</th>
                                            <th>Total Panen</th>
											<th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM kebun ORDER BY id DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										$jmlbagus = mysqli_query($konek,"SELECT SUM(jumlah_bagus) as bagus from hasil_panen where id_kebun='$row[id]'");
										$jmlkurang = mysqli_query($konek,"SELECT SUM(jumlah_KrgBagus) as kurang from hasil_panen where id_kebun='$row[id]'");

										$tk = mysqli_fetch_array($jmlbagus);
										$tb = mysqli_fetch_array($jmlkurang);

										$total = $tk['bagus'] + $tb['kurang'];
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['nama_kebun'];?></td>
                                            <td><?php echo $tk['bagus'];?></td>
                                            <td><?php echo $tb['kurang'];?></td>
											<td><?php echo $total;?></td>

                                        <?php

                                              
                                              print("
                                                <td>

                                                <a class='btn btn-warning' href=hasilTambah.php?id_=$row[id]>
                                                Update
                                                </a>
                                 
                                                </td>
                                              </tr>");
                                              

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>