<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Pemakaian</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="pemakaianTambah.php">Tambah Data</a></p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah</th>
											<th>Nama Peminjam</th>
											<th>Tanggal Pemakaian</th>
											<th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT *,peminjaman.id as kd, peminjaman.status as stat,peminjaman.jumlah as a1 FROM peminjaman,users,aset where peminjaman.id_user=users.id and peminjaman.id_barang=aset.id and peminjaman.status='1' ORDER BY peminjaman.id DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										if($row['stat']=='1'){
											$ket = "Dipinjam";
										}else{
											$ket = "Dikembalikan";
										}
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['kode_barang'];?></td>
                                            <td><?php echo $row['nama_barang'];?></td>
                                            <td><?php echo $row['a1'];?></td>
											<td><?php echo $row['nama'];?></td>
											<td><?php echo $row['tgl_peminjaman'];?></td>
											<td><?php echo $ket;?></td>

                                        <?php

                                              
                                              print("
                                                <td>
                                                <a class='btn btn-danger' href='pemakaianDelete.php?id_=$row[kd]')>
                                                Hapus
                                                </a>
												<a class='btn btn-primary' href=pemakaianKembali.php?id_=$row[kd])>
                                                Kembalikan
                                                </a>
                                                </td>
                                              </tr>");

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				<br><br>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left">Riwayat Pengembalian Barang</p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah</th>
											<th>Nama Peminjam</th>
											<th>Tanggal Pemakaian</th>
											<th>Tanggal Pengembalian</th>
											<th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT *,peminjaman.jumlah as a1 FROM peminjaman,users,aset where peminjaman.id_user=users.id and peminjaman.id_barang=aset.id and peminjaman.status='2' ORDER BY peminjaman.id DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										if($row['status']==1){
											$ket = "Dipinjam";
										}else{
											$ket = "Dikembalikan";
										}
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['kode_barang'];?></td>
                                            <td><?php echo $row['nama_barang'];?></td>
                                            <td><?php echo $row['a1'];?></td>
											<td><?php echo $row['nama'];?></td>
											<td><?php echo $row['tgl_peminjaman'];?></td>
											<td><?php echo $row['tgl_pengembalian'];?></td>
											<td><?php echo $ket;?></td>

                                        <?php

                                              
                                          

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>