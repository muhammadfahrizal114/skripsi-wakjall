-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2022 at 12:48 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_erp_sawit`
--

-- --------------------------------------------------------

--
-- Table structure for table `aset`
--

CREATE TABLE `aset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aset`
--

INSERT INTO `aset` (`id`, `nama_barang`, `kode_barang`, `jumlah`, `keterangan`, `tanggal_pembelian`, `created_at`, `updated_at`) VALUES
(3, 'Gergaji Panjang', 'GRG-01', 211, 'Pembelian gergajiii', '2021-12-17', '2021-12-17 03:13:37', '2021-12-28 08:34:15'),
(4, 'Cangkul', 'CKL-01', 1, 'Cangkul Baru', '2021-12-28', '2021-12-28 08:34:33', '2021-12-28 08:34:33'),
(16, 'Cangkul', 'PMB/AST/04-02-2022/5', 1, 'Cangkul Baru', '2021-12-28', '2022-02-04 14:18:51', '2022-02-04 14:18:51'),
(19, 'sekop', 'PMB/AST/09-02-2022/17', 3, 'untuk nyekop', '2022-02-09', '2022-02-09 10:30:58', '2022-02-09 10:30:58');

-- --------------------------------------------------------

--
-- Table structure for table `blok`
--

CREATE TABLE `blok` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_blok` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kebun` bigint(20) UNSIGNED NOT NULL,
  `panjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lebar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `luas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blok`
--

INSERT INTO `blok` (`id`, `nama_blok`, `id_kebun`, `panjang`, `lebar`, `luas`, `keterangan`, `created_at`, `updated_at`) VALUES
(7, 'A-01', 10, '100', '200', '20000', 'Blok 1 kebun rambutan', '2021-12-30 12:53:13', '2021-12-30 12:53:13'),
(8, 'A-02', 10, '100', '100', '10000', 'Blok 2 Kebun Rambutan', '2021-12-30 12:53:32', '2021-12-30 12:53:32'),
(9, 'B-12A', 10, '18', '20', '30', 'blok baru', '2021-12-31 14:20:10', '2021-12-31 14:20:10'),
(15, 'A-11', 13, '400', '1000', '1500', 'sahshsncjsaui', '2022-01-20 08:48:38', '2022-01-20 08:48:38'),
(23, 'B-12', 25, '100', '50', '40', 'menambahkan blok', '2022-07-10 09:51:53', '2022-07-10 09:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `cuaca`
--

CREATE TABLE `cuaca` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `kondisi_cuaca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cuaca`
--

INSERT INTO `cuaca` (`id`, `tanggal`, `kondisi_cuaca`, `catatan`, `created_at`, `updated_at`) VALUES
(6, '2021-12-15', 'Hujan', 'Hujan Lebat', '2021-12-15 06:55:54', '2021-12-15 06:55:54'),
(7, '2021-12-14', 'panas dalam', 'Panas Banget', '2021-12-15 06:59:37', '2021-12-15 07:18:39'),
(9, '2021-12-20', 'sedang', 'normal', '2021-12-20 06:39:35', '2021-12-20 06:39:35'),
(33, '2022-02-03', 'salju lebat', 'tanaman rusak', '2022-02-03 16:18:42', '2022-02-03 16:18:42'),
(36, '2022-06-21', 'Mendung Gerimis', 'semua aman terkendali', '2022-06-20 23:03:05', '2022-06-20 23:03:05'),
(37, '2022-07-10', 'gerimis', 'gerimis sedang', '2022-07-10 09:41:17', '2022-07-10 09:41:17');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kebun` bigint(20) UNSIGNED NOT NULL,
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `jumlah_Bagus` double(20,2) DEFAULT NULL,
  `jumlah_KrgBagus` double(20,2) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id`, `id_kebun`, `id_transaksi`, `jumlah_Bagus`, `jumlah_KrgBagus`, `harga`, `diskon`, `created_at`, `updated_at`) VALUES
(64, 10, 57, 4000000.00, 500000.00, 4725000, 5, '2022-07-10 10:34:20', '2022-07-10 10:34:20'),
(65, 13, 57, 30000000.00, 2500000.00, 35750000, 10, '2022-07-10 10:35:09', '2022-07-10 10:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groupuser`
--

CREATE TABLE `groupuser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groupuser`
--

INSERT INTO `groupuser` (`id`, `nama_group`, `created_at`, `updated_at`) VALUES
(6, 'Pimpinan', '2021-12-09 04:22:48', '2021-12-09 04:22:48'),
(11, 'Admin', '2021-12-17 09:45:21', '2021-12-17 09:45:21'),
(18, 'Pegawai', '2022-01-25 13:40:51', '2022-01-25 13:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `hama`
--

CREATE TABLE `hama` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nama_hama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkat_kerusakan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_kerusakan` int(11) NOT NULL,
  `id_kebun` bigint(20) UNSIGNED NOT NULL,
  `id_blok` bigint(20) UNSIGNED NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hama`
--

INSERT INTO `hama` (`id`, `tanggal`, `nama_hama`, `tingkat_kerusakan`, `jumlah_kerusakan`, `id_kebun`, `id_blok`, `keterangan`, `created_at`, `updated_at`) VALUES
(13, '2022-01-07', 'tikus', 'sedang', 20, 10, 7, 'lorem', '2022-01-07 15:03:34', '2022-01-07 15:03:34'),
(16, '2022-01-13', 'Tikusss', 'sedang', 12, 10, 7, 'sangat rusak', '2022-01-12 15:26:40', '2022-01-12 15:26:40'),
(17, '2022-01-14', 'jua', 'parah', 100, 10, 9, 'segera tindak lanjuti', '2022-01-12 15:27:18', '2022-01-12 15:27:18'),
(18, '2022-01-15', 'nardika', 'parah kali', 10, 10, 7, 'segera di atasi', '2022-01-12 15:27:48', '2022-01-12 15:27:48'),
(35, '2022-06-21', 'Kecoa', 'Sedang', 2, 10, 7, 'test', '2022-06-21 01:25:49', '2022-06-21 01:25:49');

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kebun` bigint(20) UNSIGNED DEFAULT NULL,
  `harga_Bagus` double DEFAULT NULL,
  `harga_KrgBagus` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id`, `id_kebun`, `harga_Bagus`, `harga_KrgBagus`, `created_at`, `updated_at`) VALUES
(1, 10, 200000, 100000, '2022-01-04 13:46:33', '2022-01-04 14:16:50'),
(9, 13, 1000000, 500000, '2022-01-13 08:01:31', '2022-01-13 08:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_panen`
--

CREATE TABLE `hasil_panen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kebun` bigint(20) UNSIGNED DEFAULT NULL,
  `id_blok` bigint(20) UNSIGNED DEFAULT NULL,
  `jumlah_bagus` double DEFAULT NULL,
  `jumlah_KrgBagus` double DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hasil_panen`
--

INSERT INTO `hasil_panen` (`id`, `id_kebun`, `id_blok`, `jumlah_bagus`, `jumlah_KrgBagus`, `total`, `tanggal`, `keterangan`, `created_at`, `updated_at`) VALUES
(24, 10, 7, 3, 2.9, 5.90, '2021-12-30', 'Blok 2 Kebun Rambutan', '2021-12-30 15:34:31', '2021-12-30 15:35:13'),
(26, 10, 8, 4, 10, 14.00, '2021-12-04', 'lppll', '2021-12-31 09:25:56', '2021-12-31 09:25:56'),
(27, 10, 8, 12, 50, 62.00, '2021-12-31', 'blok baru', '2021-12-31 09:48:06', '2022-01-12 13:25:27'),
(29, 10, 7, 30, 8.9, 10.80, '2022-01-11', 'cre gty ke', '2022-01-11 09:43:33', '2022-01-11 09:43:33'),
(30, 10, 7, 1.9, 10, 11.90, '2022-02-22', 'blok baru', '2022-01-11 10:27:59', '2022-01-12 13:26:16'),
(31, 10, 7, 10, 10, 20.00, '2022-01-13', 'ryebrcds', '2022-01-13 10:28:47', '2022-01-13 10:28:47'),
(34, 10, 7, 1.9, 8.9, 10.80, '2022-01-18', 'ghfvdrg', '2022-01-18 08:42:19', '2022-01-18 08:42:19'),
(37, 10, 9, 21, 2, 23.00, '2022-01-18', 'wqeewwf', '2022-01-18 15:17:11', '2022-01-18 15:17:11'),
(39, 10, 7, 0, 1.9, 1.90, '2022-01-19', 'zxfzd', '2022-01-19 08:59:34', '2022-01-19 08:59:34'),
(40, 10, 7, 0, 3, 3.00, '2022-01-19', 'erfgr', '2022-01-19 09:00:21', '2022-01-19 09:00:21'),
(42, 10, 9, 22, 12, 34.00, '2022-03-01', 'kebun rambutan binjai', '2022-01-20 08:40:50', '2022-01-20 08:40:50'),
(46, 13, 15, 200, 99, 299.00, '2022-07-01', 'dadkjfa', '2022-01-20 08:50:49', '2022-01-20 08:50:49'),
(49, 10, 7, 95, 65, 160.00, '2022-10-01', 'fgsbgr', '2022-01-20 08:53:13', '2022-01-20 08:53:13'),
(52, 10, 7, 5, 5, 10.00, '2022-01-22', 'test', '2022-01-22 10:34:35', '2022-01-22 10:34:35'),
(53, 10, 7, 2, 2, 4.00, '2022-01-28', 'penghasilan rambutan menurun', '2022-01-28 08:52:00', '2022-01-28 08:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `kebun`
--

CREATE TABLE `kebun` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kebun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `panjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lebar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `luas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kebun`
--

INSERT INTO `kebun` (`id`, `nama_kebun`, `panjang`, `lebar`, `luas`, `keterangan`, `created_at`, `updated_at`) VALUES
(10, 'Kebun Rambutan', '100', '100', '10000', 'hjwqiunfdc', '2021-12-30 12:01:43', '2021-12-30 12:01:43'),
(13, 'Kebun Sawit', '300', '250', '500', 'kebun kelapa sawit', '2022-01-13 07:56:21', '2022-01-13 07:56:21'),
(25, 'sawit', '100', '56', '70', 'kebun sawit milik pribadi', '2022-07-10 09:51:05', '2022-07-10 09:51:05');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_transaksi` bigint(20) UNSIGNED DEFAULT NULL,
  `id_pemasukan` bigint(20) UNSIGNED DEFAULT NULL,
  `id_pengeluaran` bigint(20) UNSIGNED DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uang_masuk` double DEFAULT NULL,
  `tanggal` date NOT NULL,
  `uang_keluar` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id`, `id_transaksi`, `id_pemasukan`, `id_pengeluaran`, `keterangan`, `uang_masuk`, `tanggal`, `uang_keluar`, `created_at`, `updated_at`) VALUES
(2, NULL, 6, NULL, 'Penjualan Bibit Durian', 1500000, '2022-01-07', 0, '2022-01-07 09:26:44', '2022-01-07 09:31:13'),
(3, NULL, NULL, 5, 'Pembelian pupuk 5kg', 0, '2022-01-07', 700000, '2022-01-07 09:40:12', '2022-01-07 09:40:36'),
(8, NULL, 8, NULL, 'Sewa Cangkul', 90000, '2022-01-10', 0, '2022-01-10 14:32:08', '2022-01-10 14:32:44'),
(9, NULL, NULL, 6, 'Beli Pisau', 0, '2022-01-10', 20000, '2022-01-10 15:25:40', '2022-01-10 15:25:55'),
(32, NULL, 13, NULL, 'Pak adi membantu tetangga sebelah', 150000, '2022-01-13', 0, '2022-01-13 14:15:13', '2022-01-13 14:15:13'),
(66, NULL, 20, NULL, 'pinjam sementara', 12000, '2022-02-14', 0, '2022-02-14 09:22:17', '2022-02-14 09:22:17'),
(67, NULL, NULL, 15, 'bulan pertama', 0, '2022-02-14', 500000, '2022-02-14 14:01:19', '2022-02-14 14:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', NULL, NULL),
(2, 'Cuaca', NULL, NULL),
(3, 'Hama', NULL, NULL),
(4, 'Pupuk', NULL, NULL),
(5, 'Penjualan', NULL, NULL),
(6, 'User', NULL, NULL),
(7, 'Permintaan', NULL, NULL),
(8, 'Pemakaian', NULL, NULL),
(9, 'Persetujuan', NULL, NULL),
(10, 'Hasil Panen', NULL, NULL),
(11, 'Laporan', NULL, NULL),
(12, 'Kebun', NULL, NULL),
(13, 'Aset', NULL, NULL),
(14, 'Pengeluaran', NULL, NULL),
(15, 'Harga', NULL, NULL),
(16, 'Level Kuasa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Otoritas', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2021_12_04_022823_groupuser', 2),
(5, '2014_10_12_000000_create_users_table', 3),
(6, '2021_12_07_072306_cuaca', 4),
(11, '2014_10_12_100000_create_password_resets_table', 7),
(12, '2019_08_19_000000_create_failed_jobs_table', 7),
(13, '2019_12_14_000001_create_personal_access_tokens_table', 7),
(15, '2021_12_08_062911_kebun', 8),
(17, '2021_12_08_064048_blok', 9),
(19, '2021_12_09_012538_hama', 10),
(20, '2021_12_17_020036_pupuk', 11),
(22, '2021_12_17_023427_aset', 13),
(23, '2021_12_20_063819_menu', 14),
(24, '2021_12_20_065905_tipe_otorisasi', 15),
(28, '2021_12_20_065523_otorisasi', 16),
(30, '2021_12_20_030054_create_permission_tables', 18),
(31, '2021_12_23_024350_permintaan', 19),
(41, '2021_12_29_063520_hasilpanen', 21),
(42, '2022_01_04_095627_harga', 22),
(55, '2022_01_04_095716_transaksi', 34),
(56, '2022_01_04_095751_pembayaran', 35),
(57, '2022_01_04_095736_detail_transaksi', 36),
(59, '2022_01_06_082925_pemasukan', 37),
(60, '2022_01_06_093801_pengeluaran', 38),
(62, '2022_01_07_085457_laporan', 39),
(63, '2021_12_17_020233_pemupukan', 40),
(64, '2021_12_27_030643_peminjaman', 41);

-- --------------------------------------------------------

--
-- Table structure for table `otorisasi`
--

CREATE TABLE `otorisasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_groupuser` bigint(20) UNSIGNED NOT NULL,
  `id_menu` bigint(20) UNSIGNED NOT NULL,
  `id_tipeOtorisasi` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otorisasi`
--

INSERT INTO `otorisasi` (`id`, `id_groupuser`, `id_menu`, `id_tipeOtorisasi`, `created_at`, `updated_at`) VALUES
(3024, 6, 1, 1, NULL, NULL),
(3025, 6, 1, 2, NULL, NULL),
(3026, 6, 1, 3, NULL, NULL),
(3027, 6, 1, 4, NULL, NULL),
(3028, 6, 2, 1, NULL, NULL),
(3029, 6, 3, 1, NULL, NULL),
(3030, 6, 4, 1, NULL, NULL),
(3031, 6, 5, 1, NULL, NULL),
(3032, 6, 5, 2, NULL, NULL),
(3033, 6, 5, 3, NULL, NULL),
(3034, 6, 5, 4, NULL, NULL),
(3035, 6, 6, 1, NULL, NULL),
(3036, 6, 6, 2, NULL, NULL),
(3037, 6, 6, 3, NULL, NULL),
(3038, 6, 6, 4, NULL, NULL),
(3039, 6, 7, 1, NULL, NULL),
(3040, 6, 9, 1, NULL, NULL),
(3041, 6, 9, 2, NULL, NULL),
(3042, 6, 9, 3, NULL, NULL),
(3043, 6, 9, 4, NULL, NULL),
(3044, 6, 10, 1, NULL, NULL),
(3045, 6, 11, 1, NULL, NULL),
(3046, 6, 12, 1, NULL, NULL),
(3047, 6, 13, 1, NULL, NULL),
(3048, 6, 14, 1, NULL, NULL),
(3049, 6, 15, 1, NULL, NULL),
(3050, 6, 16, 1, NULL, NULL),
(3051, 6, 16, 2, NULL, NULL),
(3052, 6, 16, 3, NULL, NULL),
(3053, 6, 16, 4, NULL, NULL),
(3054, 6, 17, 1, NULL, NULL),
(3055, 6, 17, 2, NULL, NULL),
(3056, 6, 17, 3, NULL, NULL),
(3057, 6, 17, 4, NULL, NULL),
(3058, 11, 1, 1, NULL, NULL),
(3059, 11, 1, 2, NULL, NULL),
(3060, 11, 1, 3, NULL, NULL),
(3061, 11, 1, 4, NULL, NULL),
(3062, 11, 2, 1, NULL, NULL),
(3063, 11, 2, 2, NULL, NULL),
(3064, 11, 2, 3, NULL, NULL),
(3065, 11, 2, 4, NULL, NULL),
(3066, 11, 3, 1, NULL, NULL),
(3067, 11, 3, 2, NULL, NULL),
(3068, 11, 3, 3, NULL, NULL),
(3069, 11, 3, 4, NULL, NULL),
(3070, 11, 4, 1, NULL, NULL),
(3071, 11, 4, 2, NULL, NULL),
(3072, 11, 4, 3, NULL, NULL),
(3073, 11, 4, 4, NULL, NULL),
(3074, 11, 5, 1, NULL, NULL),
(3075, 11, 5, 2, NULL, NULL),
(3076, 11, 5, 3, NULL, NULL),
(3077, 11, 5, 4, NULL, NULL),
(3078, 11, 6, 1, NULL, NULL),
(3079, 11, 6, 2, NULL, NULL),
(3080, 11, 6, 3, NULL, NULL),
(3081, 11, 6, 4, NULL, NULL),
(3082, 11, 7, 1, NULL, NULL),
(3083, 11, 7, 2, NULL, NULL),
(3084, 11, 7, 3, NULL, NULL),
(3085, 11, 7, 4, NULL, NULL),
(3086, 11, 8, 1, NULL, NULL),
(3087, 11, 8, 2, NULL, NULL),
(3088, 11, 8, 3, NULL, NULL),
(3089, 11, 8, 4, NULL, NULL),
(3090, 11, 10, 1, NULL, NULL),
(3091, 11, 10, 2, NULL, NULL),
(3092, 11, 10, 3, NULL, NULL),
(3093, 11, 10, 4, NULL, NULL),
(3094, 11, 11, 1, NULL, NULL),
(3095, 11, 11, 2, NULL, NULL),
(3096, 11, 11, 3, NULL, NULL),
(3097, 11, 11, 4, NULL, NULL),
(3098, 11, 12, 1, NULL, NULL),
(3099, 11, 12, 2, NULL, NULL),
(3100, 11, 12, 3, NULL, NULL),
(3101, 11, 12, 4, NULL, NULL),
(3102, 11, 13, 1, NULL, NULL),
(3103, 11, 13, 2, NULL, NULL),
(3104, 11, 13, 3, NULL, NULL),
(3105, 11, 13, 4, NULL, NULL),
(3106, 11, 14, 1, NULL, NULL),
(3107, 11, 14, 2, NULL, NULL),
(3108, 11, 14, 3, NULL, NULL),
(3109, 11, 14, 4, NULL, NULL),
(3110, 11, 15, 1, NULL, NULL),
(3111, 11, 15, 2, NULL, NULL),
(3112, 11, 15, 3, NULL, NULL),
(3113, 11, 15, 4, NULL, NULL),
(3114, 18, 1, 1, NULL, NULL),
(3115, 18, 1, 2, NULL, NULL),
(3116, 18, 1, 3, NULL, NULL),
(3117, 18, 1, 4, NULL, NULL),
(3118, 18, 2, 1, NULL, NULL),
(3119, 18, 2, 2, NULL, NULL),
(3120, 18, 2, 3, NULL, NULL),
(3121, 18, 2, 4, NULL, NULL),
(3122, 18, 3, 1, NULL, NULL),
(3123, 18, 3, 2, NULL, NULL),
(3124, 18, 3, 3, NULL, NULL),
(3125, 18, 3, 4, NULL, NULL),
(3126, 18, 4, 1, NULL, NULL),
(3127, 18, 4, 2, NULL, NULL),
(3128, 18, 4, 3, NULL, NULL),
(3129, 18, 4, 4, NULL, NULL),
(3130, 18, 8, 1, NULL, NULL),
(3131, 18, 8, 2, NULL, NULL),
(3132, 18, 8, 3, NULL, NULL),
(3133, 18, 8, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pemasukan`
--

CREATE TABLE `pemasukan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemasukan`
--

INSERT INTO `pemasukan` (`id`, `keterangan`, `jumlah`, `tanggal`, `created_at`, `updated_at`) VALUES
(6, 'Penjualan Bibit Durian', 1500000, '2022-01-07', '2022-01-07 09:26:44', '2022-01-07 09:31:13'),
(8, 'Sewa Cangkul', 90000, '2022-01-10', '2022-01-10 14:32:08', '2022-01-10 14:32:44'),
(13, 'Pak adi membantu tetangga sebelah', 150000, '2022-01-13', '2022-01-13 14:15:13', '2022-01-13 14:15:13'),
(20, 'pinjam sementara', 12000, '2022-02-14', '2022-02-14 09:22:17', '2022-02-14 09:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `bayar` double DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_transaksi`, `bayar`, `tanggal`, `created_at`, `updated_at`) VALUES
(50, 57, 475000, '2022-07-13', '2022-07-10 10:35:42', '2022-07-10 10:35:42'),
(51, 57, 4000000, '2022-07-15', '2022-07-10 10:36:12', '2022-07-10 10:36:12'),
(52, 57, 36000000, '2022-07-18', '2022-07-10 10:36:39', '2022-07-10 10:36:39');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `id_barang` bigint(20) UNSIGNED NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_peminjaman` date NOT NULL,
  `tgl_pengembalian` date DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `id_user`, `id_barang`, `jumlah`, `tgl_peminjaman`, `tgl_pengembalian`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(1, 39, 3, '1', '2022-01-24', '2022-01-27', 'asd', '2', '2022-01-24 10:16:07', '2022-01-27 10:07:05'),
(6, 39, 4, '1', '2022-01-25', '2022-01-25', 'asd', '2', '2022-01-25 13:52:40', '2022-01-25 13:52:44'),
(8, 51, 3, '2', '2022-01-27', '2022-01-27', 'kkjohj', '2', '2022-01-27 13:39:24', '2022-01-27 15:45:52'),
(17, 52, 4, '13', '2022-02-03', '2022-02-03', 'baruu', '2', '2022-02-03 10:18:11', '2022-02-03 10:48:14'),
(25, 51, 4, '1', '2022-02-08', NULL, 'ADSASW', '1', '2022-02-08 08:21:31', '2022-02-08 08:21:31'),
(26, 51, 3, '5', '2022-02-08', NULL, 'UYJH', '1', '2022-02-08 08:21:31', '2022-02-08 08:21:31'),
(27, 55, 4, '12', '2022-02-09', '2022-02-09', 'buat menggali parit', '2', '2022-02-09 08:35:24', '2022-02-09 08:35:33'),
(28, 55, 3, '2', '2022-02-09', '2022-02-09', 'pinjam', '2', '2022-02-09 08:36:21', '2022-02-09 08:36:56'),
(30, 51, 19, '1', '2022-02-09', '2022-02-09', NULL, '2', '2022-02-09 10:31:28', '2022-02-09 10:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `pemupukan`
--

CREATE TABLE `pemupukan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pupuk` bigint(20) UNSIGNED NOT NULL,
  `id_kebun` bigint(20) UNSIGNED NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemupukan`
--

INSERT INTO `pemupukan` (`id`, `id_pupuk`, `id_kebun`, `jumlah`, `tanggal`, `keterangan`, `satuan`, `created_at`, `updated_at`) VALUES
(3, 14, 10, 12, '2022-01-14', 'afsdfd', 'Kg', '2022-01-14 14:57:33', '2022-01-14 14:57:33'),
(6, 20, 13, 23, '2022-01-15', 'dgshsfh', 'Kg', '2022-01-14 14:58:24', '2022-01-14 14:58:24'),
(9, 22, 13, 123, '2022-01-18', 'essese', 'Kg', '2022-01-18 09:50:01', '2022-01-18 09:50:01'),
(14, 12, 10, 8, '2022-02-07', 'mm', 'Kg', '2022-02-07 09:32:36', '2022-02-07 09:32:36'),
(18, 13, 13, 12, '2022-07-10', 'memupupuk blok b-12', 'L', '2022-07-10 09:42:25', '2022-07-10 09:42:25');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` double NOT NULL,
  `tanggal` date NOT NULL,
  `nota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`id`, `keterangan`, `jumlah`, `tanggal`, `nota`, `created_at`, `updated_at`) VALUES
(1, 'Pembelian 10 cangkul', 2000000, '2022-01-06', '1641437905.jpg', '2022-01-06 09:58:25', '2022-01-06 09:58:25'),
(5, 'Pembelian pupuk 5kg', 700000, '2022-01-07', '1641523212.jpg', '2022-01-07 09:40:12', '2022-01-07 09:40:36'),
(6, 'Beli Pisau', 20000, '2022-01-10', '1641803140.png', '2022-01-10 15:25:40', '2022-01-10 15:25:55'),
(15, 'bulan pertama', 500000, '2022-02-14', '1644822079.png', '2022-02-14 14:01:19', '2022-02-14 14:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE `permintaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pupuk`
--

CREATE TABLE `pupuk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pupuk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pupuk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pupuk`
--

INSERT INTO `pupuk` (`id`, `nama_pupuk`, `jenis_pupuk`, `created_at`, `updated_at`) VALUES
(12, 'Urea', 'Cair', '2021-12-17 07:18:56', '2021-12-17 07:18:56'),
(13, 'Amonia', 'Cair', '2021-12-17 07:19:16', '2021-12-17 07:19:16'),
(14, 'Nitrat', 'Cair', '2021-12-17 07:20:08', '2021-12-17 07:20:08'),
(18, 'wahyu', 'wahyu', '2021-12-20 09:17:32', '2021-12-20 09:17:32'),
(19, 'qwer', 'qwer', '2021-12-21 07:27:31', '2021-12-21 07:27:31'),
(20, 'organik', 'Cair', '2021-12-21 07:27:46', '2021-12-21 07:27:46'),
(21, 'kompos', 'organik', '2022-01-11 14:13:49', '2022-01-11 14:13:49'),
(22, 'HCL', 'Cair', '2022-01-13 09:26:50', '2022-01-13 09:26:50'),
(23, 'Sianida', 'Cair', '2022-01-13 13:57:11', '2022-01-13 13:57:11'),
(24, 'Urea', 'Cair', '2022-01-22 09:37:21', '2022-01-22 09:37:21'),
(25, 'Urin', 'Cair', '2022-01-22 10:09:49', '2022-01-22 10:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_transaksi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembeli` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_lunas` tinyint(1) NOT NULL,
  `jumlah_Bayar` double DEFAULT NULL,
  `total_harga` double DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_transaksi`, `pembeli`, `is_lunas`, `jumlah_Bayar`, `total_harga`, `tanggal`, `created_at`, `updated_at`) VALUES
(57, 'TRX/20220710/001', 'Fahrizal', 0, 4725000, 40475000, '2022-07-10', '2022-07-10 10:34:20', '2022-07-10 10:34:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('aktif','nonaktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_groupuser` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `status`, `nama`, `email`, `telepon`, `alamat`, `image`, `nik`, `id_groupuser`, `remember_token`, `created_at`, `updated_at`) VALUES
(39, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'aktif', 'Rio juan hendri butar-butar', 'rolandjuan09@gmail.com', '081261538826', 'batu aji', '20220203085535.png', '210744362326754', 11, NULL, '2021-12-22 06:09:18', '2022-02-03 08:55:35'),
(51, 'nora', '$2y$10$GCue9vKTRt2oCrDPkijefumFGAVmYZlh66xyHbybfUqGrSwMmyE/.', 'aktif', 'nora', 'nora@gmail.com', '047817484148141', 'bengkong', '20220211100037.jfif', '63167141719417412', 18, NULL, '2022-01-26 15:11:15', '2022-02-11 10:00:37'),
(52, 'falmi', '21232f297a57a5a743894a0e4a801fc3', 'aktif', 'falmi', 'nardika@gmail.com', '93849283', 'dimana', '20220203103214.png', '28939283928392', 6, NULL, '2022-02-03 10:09:16', '2022-02-03 10:50:03'),
(53, 'rina', '$2y$10$mf.W7PCRX8fH7pd1ny8VO.szHtRle/euyyIY9DrE.HfBPlzJ04uj2', 'aktif', 'rina', 'wakjal@gmail.com', '0874474117', 'batam', '20220211100023.jfif', '12414612912', 6, NULL, '2022-02-07 08:23:03', '2022-02-11 10:00:23'),
(55, 'fahri', '$2y$10$.180uc82ioFfAITLlBqr0e0yqMh4e85fjbWMIYp0KHME5.aonE6Pe', 'aktif', 'Muhammad Fahrizal', 'wakjal12@gmail.com', '08412741896858', 'pekanbaru', '20220211100000.jfif', '124912498111278917', 11, NULL, '2022-02-08 08:37:40', '2022-02-11 10:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aset`
--
ALTER TABLE `aset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blok`
--
ALTER TABLE `blok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blok_id_kebun_foreign` (`id_kebun`);

--
-- Indexes for table `cuaca`
--
ALTER TABLE `cuaca`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transaksi_id_kebun_foreign` (`id_kebun`),
  ADD KEY `detail_transaksi_id_transaksi_foreign` (`id_transaksi`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `groupuser`
--
ALTER TABLE `groupuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hama`
--
ALTER TABLE `hama`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hama_id_kebun_foreign` (`id_kebun`),
  ADD KEY `hama_id_blok_foreign` (`id_blok`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `harga_id_kebun_foreign` (`id_kebun`);

--
-- Indexes for table `hasil_panen`
--
ALTER TABLE `hasil_panen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hasil_panen_id_kebun_foreign` (`id_kebun`),
  ADD KEY `hasil_panen_id_blok_foreign` (`id_blok`);

--
-- Indexes for table `kebun`
--
ALTER TABLE `kebun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laporan_id_transaksi_foreign` (`id_transaksi`),
  ADD KEY `laporan_id_pemasukan_foreign` (`id_pemasukan`),
  ADD KEY `laporan_id_pengeluaran_foreign` (`id_pengeluaran`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otorisasi`
--
ALTER TABLE `otorisasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `otorisasi_id_groupuser_foreign` (`id_groupuser`),
  ADD KEY `otorisasi_id_menu_foreign` (`id_menu`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pemasukan`
--
ALTER TABLE `pemasukan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembayaran_id_transaksi_foreign` (`id_transaksi`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `peminjaman_id_user_foreign` (`id_user`),
  ADD KEY `peminjaman_id_barang_foreign` (`id_barang`);

--
-- Indexes for table `pemupukan`
--
ALTER TABLE `pemupukan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pemupukan_id_kebun_foreign` (`id_kebun`),
  ADD KEY `pemupukan_id_pupuk_foreign` (`id_pupuk`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permintaan_id_user_foreign` (`id_user`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `pupuk`
--
ALTER TABLE `pupuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_groupuser_foreign` (`id_groupuser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aset`
--
ALTER TABLE `aset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `blok`
--
ALTER TABLE `blok`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cuaca`
--
ALTER TABLE `cuaca`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groupuser`
--
ALTER TABLE `groupuser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `hama`
--
ALTER TABLE `hama`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hasil_panen`
--
ALTER TABLE `hasil_panen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `kebun`
--
ALTER TABLE `kebun`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `otorisasi`
--
ALTER TABLE `otorisasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3134;

--
-- AUTO_INCREMENT for table `pemasukan`
--
ALTER TABLE `pemasukan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pemupukan`
--
ALTER TABLE `pemupukan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pupuk`
--
ALTER TABLE `pupuk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blok`
--
ALTER TABLE `blok`
  ADD CONSTRAINT `blok_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksi_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hama`
--
ALTER TABLE `hama`
  ADD CONSTRAINT `hama_id_blok_foreign` FOREIGN KEY (`id_blok`) REFERENCES `blok` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hama_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `harga_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hasil_panen`
--
ALTER TABLE `hasil_panen`
  ADD CONSTRAINT `hasil_panen_id_blok_foreign` FOREIGN KEY (`id_blok`) REFERENCES `blok` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_panen_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `laporan`
--
ALTER TABLE `laporan`
  ADD CONSTRAINT `laporan_id_pemasukan_foreign` FOREIGN KEY (`id_pemasukan`) REFERENCES `pemasukan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `laporan_id_pengeluaran_foreign` FOREIGN KEY (`id_pengeluaran`) REFERENCES `pengeluaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `laporan_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `otorisasi`
--
ALTER TABLE `otorisasi`
  ADD CONSTRAINT `otorisasi_id_groupuser_foreign` FOREIGN KEY (`id_groupuser`) REFERENCES `groupuser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `otorisasi_id_menu_foreign` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `aset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjaman_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemupukan`
--
ALTER TABLE `pemupukan`
  ADD CONSTRAINT `pemupukan_id_kebun_foreign` FOREIGN KEY (`id_kebun`) REFERENCES `kebun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemupukan_id_pupuk_foreign` FOREIGN KEY (`id_pupuk`) REFERENCES `pupuk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD CONSTRAINT `permintaan_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_groupuser_foreign` FOREIGN KEY (`id_groupuser`) REFERENCES `groupuser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
