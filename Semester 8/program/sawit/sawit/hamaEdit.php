<?php

ini_set("display_errors","Off");
include "header.php";

include("connect.php");

?>


    <?php
    
    $id=$_GET['id_'];
    $show_kelas="SELECT * FROM hama WHERE id='$id'";
    $hasil_kelas=mysqli_query($konek,$show_kelas);
    $view_kelas=mysqli_fetch_array($hasil_kelas);
    
    ?>


<div class="content-wrapper">
        <div class="container"><br><br>
              <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Ubah Data Hama </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                        
                        <div class="panel-body">


                            <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl" value="<?php echo $view_kelas['tanggal'] ?>"/>
                                          </div>
										  <div class="form-group">
                                            <label>Nama Hama</label>
                                            <input type="text" class="form-control" name="hama" value="<?php echo $view_kelas['nama_hama'] ?>"/>
                                          </div>
                                          <div class="form-group">
										    <label>Tingkat Kerusakan</label>
                                            <input type="text" class="form-control" name="rusak" value="<?php echo $view_kelas['tingkat_kerusakan'] ?>"/>
                                          </div>
                                          <div class="form-group">
										    <label>Jumlah Kerusakan</label>
                                            <input type="number" class="form-control" name="jml" value="<?php echo $view_kelas['jumlah_kerusakan'] ?>"/>
                                          </div>
										  <div class="form-group">
										    <label>Kebun</label>
											<select name="kebun" id="kebun" class="form-control">
												<option>--Pilih Kebun--</option>
												<?php
												$sql = mysqli_query($konek,"SELECT * FROM kebun where id='$view_kelas[id_kebun]'");
												$row = mysqli_fetch_array($sql);
												?>
												<option value="<?php echo $row['id'] ?>" selected><?php echo $row['nama_kebun']; ?></option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM kebun");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_kebun']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
                                            <label>Blok</label>
											<select name="blok" id="blok" class="form-control">
												<option>--Pilih Blok--</option>
												<?php
												$sql = mysqli_query($konek,"SELECT * FROM blok where id='$view_kelas[id_blok]'");
												$row = mysqli_fetch_array($sql);
												?>
												<option value="<?php echo $row['id'] ?>" selected><?php echo $row['nama_blok']; ?></option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM blok");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_blok']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
										    <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" cols="10" rows="8"><?php echo $view_kelas['keterangan'] ?></textarea>
                                          </div>
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="kriteria.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $tgl=$_POST['tgl'];
                                      $hama=$_POST['hama'];
                                      $rusak=$_POST['rusak'];
									  $jml=$_POST['jml'];
									  $kebun=$_POST['kebun'];
									  $blok=$_POST['blok'];
									  $ket=$_POST['ket'];
                                      

                                      if(isset($tgl,$hama)){
                                        if((!$tgl)||(!$hama)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="update hama set tanggal='$tgl',nama_hama='$hama',tingkat_kerusakan='$rusak',jumlah_kerusakan='$jml',id_kebun='$kebun',id_blok='$blok',keterangan='$ket' where id='$id'";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Diubah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=hama.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
</script>
<script type="text/javascript">
	var htmlobjek;
	$(document).ready(function(){
		//apabila terjadi onchange pada poli id
		$("#kebun").change(function(){
			var poli = $("#kebun").val();
			$.ajax({
				url:"ambilBlok.php",
				data:"blok="+poli,
				cache:false,
				success:function(msg){
					$("#blok").html(msg);
				}
			});
		});
	});
</script>



