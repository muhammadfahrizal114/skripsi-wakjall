<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Cuaca</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="penjualanTambah.php">Penjualan Hasil Panen</a></p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Nama Pembeli</th>
                                            <th>Jumlah Dibayar</th>
											<th>Total Harga</th>
											<th>Sisa</th>
											<th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT distinct(pembeli),tanggal,is_lunas,id FROM transaksi ORDER BY tanggal DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										$sqlb=mysqli_query($konek,"SELECT SUM(bayar) as total FROM pembayaran where id_transaksi='$row[id]'");
										$bay = mysqli_fetch_array($sqlb);
										$query=mysqli_query($konek,"SELECT SUM(harga) as  total1 FROM detail_transaksi where id_transaksi='$row[id]'");
										$ttl = mysqli_fetch_array($query);
										$sisa = $ttl['total1'] - $bay['total'];
										
										if($sisa==0){
										    mysqli_query($konek,"update transaksi set is_lunas='1' where id='$row[id]'");
										}
										
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['tanggal'];?></td>
                                            <td><?php echo $row['pembeli'];?></td>
                                            <td><?php if($bay['total']==''){echo '0';}else{echo $bay['total'];}?></td>
											<td><?php echo $ttl['total1'];?></td>
											<td><?php echo $sisa; ?></td>
											<td><?php if($row['is_lunas']=='1'){ echo 'Lunas'; }else{ echo 'Hutang';} ?></td>
                                        <?php
											
											  if($row['is_lunas']==1){
                                              
                                              print("
                                                <td>
                                                <a class='btn btn-success' href=javascript:KonfirmasiHapus('detailPenjualan.php?id_','$row[id]')>
                                                Detail
                                                </a>
                                                </td>
                                              </tr>");
											  }else{
											   print("
                                                <td>
												<a class='btn btn-success' href=transaksiTambah.php?id_=$row[id]>
                                                Tambah
                                                </a>
                                                <a class='btn btn-primary' href=bayar.php?id_=$row[id]>
                                                Bayar
                                                </a>
												<a class='btn btn-warning' href=kwitansi.php?id_=$row[id]>
                                                Kwitnsi
                                                </a>
                                                <a class='btn btn-danger' href='penjualanDelete.php?id_=$row[id]'>
                                                Hapus
                                                </a>
                                                </td>
                                              </tr>");
											  }
                                              

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <p align="left"><a class='btn btn-primary' href="pemasukanTambah.php">Pemasukan Lainnya</a></p>
                                <div class="table-responsive">
                                <table id="datatable1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM pemasukan ORDER BY tanggal DESC");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['tanggal'];?></td>
                                            <td><?php echo $row['keterangan'];?></td>
                                            <td><?php echo $row['jumlah'];?></td>
                                        <?php
											
											 
											   print("
                                                <td>
												<a class='btn btn-warning' href=pemasukanEdit.php?id_=$row[id]>
                                                Ubah
                                                </a>
                                                <a class='btn btn-danger' href='pemasukanDelete.php?id_=$row[id]'>
                                                Hapus
                                                </a>
                                                </td>
                                              </tr>");
											                                                

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
				$("#datatable1").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>