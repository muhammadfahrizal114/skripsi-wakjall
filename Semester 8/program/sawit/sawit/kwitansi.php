<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Transaksi Penjualan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Transaksi</li>
                        </ol>
                    </div>
                   
                </div>
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
							<?php
							$dta=mysqli_fetch_array(mysqli_query($konek,"SELECT * FROM transaksi where id='$_GET[id_]'"));
							?>
								<div align="center"><h2>KWITANSI</h2></div><br><br>
                                <div class="table-responsive">
								<table width="100%">
									<tr>
									  <td valign="top" width="30%">
									  <table>
										<tr><td>Dari</td><td>:</td></tr>
										<tr><td colspan="2">PT. XXX</td></tr>
									  </table>
									  </td>
									   <td valign="top" width="30%">
									  <table>
										<tr><td>Kepada</td><td>:</td></tr>
										<tr><td colspan="2"><?= $dta['pembeli']; ?></td></tr>
									  </table>
									   </td>
									  <td valign="top" align="right" width="40%">
									  <table>
										<tr><td>Kode Transaksi</td><td>:</td><td><?= $dta['kode_transaksi']; ?></td></tr>
										<tr><td>Tanggal</td><td>:</td><td><?= $dta['tanggal']; ?></td></tr>
									  </table>
									  </td>
									</tr>
								</table>
								<br><br>
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kebun</th>
                                            <th>Harga Bagus</th>
											<th>Harga Kurang Bagus</th>
											<th>Diskon</th>
											<th>Total Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM detail_transaksi where id_transaksi='$_GET[id_]'");
										$ttl=mysqli_fetch_array(mysqli_query($konek,"SELECT * FROM transaksi where id='$_GET[id_]'"));

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										$kbn = mysqli_fetch_array(mysqli_query($konek,"select * from kebun where id='$row[id_kebun]'"));
								        $jml_bayar = ($kbn['harga_Bagus'] + $kbn['harga_KrgBagus']);
									    $ttl_disk = $jml_bayar * ($disk/100);
									    $total = $jml_bayar + $ttl_disk;
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $kbn['nama_kebun'];?></td>
                                            <td><?php echo round($row['jumlah_Bagus'],2);?></td>
											<td><?php echo round($row['jumlah_KrgBagus'],2);?></td>
											<td><?php echo $row['diskon'];?></td>
											<td><?php echo $row['harga'];?></td>
                                          </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
								<table width="100%">
									<tr>
									  <td valign="top" width="30%">
									  
									  </td>
									   <td valign="top" width="30%">
									 
									   </td>
									  <td valign="top" align="right" width="40%">
									  <hr></hr>
									  <h2>TOTAL : <?= $ttl['total_harga']; ?></h2>
									  <hr></hr>
									  </td>
									</tr>
								</table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				
				
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
				$("#datatable1").dataTable();
            });
        </script>    
<?php
  include "footer.php";
?>