<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH DATA USER</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah User</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
                                            <label>NIK</label>
                                            <input type="text" class="form-control" name="nik"/>
                                          </div>
										  <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="nama"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Telp</label>
                                            <input type="text" class="form-control" name="telp"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Alamat</label>
                                            <input type="text" class="form-control" name="alamat"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" name="user"/>
                                          </div>
                                          <div class="form-group">
                                            <label>Passsword</label>
                                            <input type="password" class="form-control" name="pass"/>
                                          </div>
                                          <div class="form-group">
                                            <label>level Kuasa</label>
                                            <select name="level" id="" class="form-control">
												<option>--Pilih Level Kuasa--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM groupuser");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_group']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
                                          <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" id="" class="form-control">
												<option>--Pilih Kebun--</option>
												<option value="1">Aktif</option>
												<option value="2">Non Aktif</option>
										   </select>
                                          </div>
                                       
                                          <div class="form-group">
										    <label>Foto</label>
                                            <input type="file" class="form-control" name="foto" />
                                          </div>
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="kriteria.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $nik=$_POST['nik'];
                                      $nama=$_POST['nama'];
                                      $telp=$_POST['telp'];
                                      $alamat=$_POST['alamat'];
                                      $email=$_POST['email'];
                                      $user=$_POST['user'];
                                      $pass=$_POST['pas'];
                                      $level=$_POST['level'];
                                      $stat=$_POST['status'];
                                      
                                      $file=$_FILES['foto']['name'];
                                      

                                      if(isset($nik,$nama)){
                                        if((!$nik)||(!$nama)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO users VALUES ('','$user','$pass','$stat','$nama','$email','$telp','$alamat','$file','$nik','$level','',NOW(),NOW())";
                                      move_uploaded_file ($_FILES['foto']['tmp_name'], "upload/".$file);
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=user.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>