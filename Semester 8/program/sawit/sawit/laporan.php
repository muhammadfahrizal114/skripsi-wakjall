<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Cuaca</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
								<p align="left">Transaksi Penjualan</p>
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>Uang Masuk</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT *, pembayaran.tanggal as tglBayar FROM pembayaran,transaksi where pembayaran.id_transaksi=transaksi.id ORDER BY pembayaran.tanggal DESC");
										$masuk1=mysqli_query($konek,"SELECT SUM(pembayaran.bayar) as jml FROM pembayaran,transaksi where pembayaran.id_transaksi=transaksi.id ORDER BY pembayaran.tanggal DESC");

                                        $no=1;
										$totalmasuk1 = mysqli_fetch_array($masuk1);

                                        while ($row=mysqli_fetch_array($sql)){?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['kode_transaksi'];?></td>
                                            <td><?php echo $row['tglBayar'];?></td>
                                            <td><?php echo $row['bayar'];?></td>

                                        <?php

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div><br><br>
							<p align="left">Pemasukan Lainnya</p>
                                <div class="table-responsive">
                                <table id="datatable2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>Uang Masuk</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sqlmasuk=mysqli_query($konek,"SELECT * from pemasukan ORDER BY tanggal DESC");
										$masuk2=mysqli_query($konek,"SELECT SUM(jumlah) as jmlain from pemasukan ORDER BY tanggal DESC");
										$totalmasuk2 = mysqli_fetch_array($masuk2);

                                        $no=1;
										$pemasukan = $totalmasuk1['jml'] + $totalmasuk2['jmlain'];
                                        while ($dta=mysqli_fetch_array($sqlmasuk)){?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $dta['keterangan'];?></td>
                                            <td><?php echo $dta['tanggal'];?></td>
                                            <td><?php echo $dta['jumlah'];?></td>

                                        <?php

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
							<br>
							<a class='btn btn-primary' href="">Total Pemasukan : <?php echo $pemasukan; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
				
				<br>
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
							<p align="left">Pengeluaran</p>
                                <div class="table-responsive">
                                <table id="datatable2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                            <th>Uang Masuk</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sqlkeluar=mysqli_query($konek,"SELECT * from pengeluaran ORDER BY tanggal DESC");
										$keluar=mysqli_query($konek,"SELECT SUM(jumlah) as jmlain from pengeluaran ORDER BY tanggal DESC");
										$totalkeluar = mysqli_fetch_array($keluar);

                                        $no=1;
										$pengeluaran = $totalkeluar['jmlain'];
                                        while ($dt=mysqli_fetch_array($sqlkeluar)){?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $dt['keterangan'];?></td>
                                            <td><?php echo $dt['tanggal'];?></td>
                                            <td><?php echo $dt['jumlah'];?></td>

                                        <?php

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
							<br>
							<a class='btn btn-primary' href="">Total Pengeluaran : <?php echo $pengeluaran; ?></a>
							<br><br>
							
							<a class='btn btn-warning' href="">Uang Khas : <?php echo $pemasukan - $pengeluaran; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
				$("#datatable2").dataTable();
            });
        </script>    
    
<?php
  include "footer.php";
?>