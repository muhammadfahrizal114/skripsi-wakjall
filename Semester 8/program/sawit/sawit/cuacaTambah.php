<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH AKTIFITAS CUACA</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah Cuaca</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl"/>
                                          </div>
										  <div class="form-group">
                                            <label>Nama Cuaca</label>
                                            <input type="text" class="form-control" name="cuaca"/>
                                          </div>
                                          <div class="form-group">
										    <label>Catatan</label>
                                            <textarea class="form-control" name="catatan" cols="10" rows="8"></textarea>
                                          </div>
                                        
                                          
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="kriteria.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $tgl=$_POST['tgl'];
                                      $cuaca=$_POST['cuaca'];
                                      $catatan=$_POST['catatan'];
                                      

                                      if(isset($tgl,$cuaca)){
                                        if((!$tgl)||(!$cuaca)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO cuaca VALUES ('','$tgl','$cuaca','$catatan',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=cuaca.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>