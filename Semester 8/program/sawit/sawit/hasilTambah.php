<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH DATA HASIL PANEN</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah Hasil Panen</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
				  <?php
				  $kbn = mysqli_fetch_array(mysqli_query($konek,"SELECT * FROM kebun where id='$_GET[id_]'"));
				  $kj = mysqli_fetch_array(mysqli_query($konek,"SELECT sum(total) as hasil from hasil_panen where id_kebun='$_GET[id_]'"));
				  ?>
                                           
                                          <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl"/>
                                          </div>
										  <div class="form-group">
                                            <label>Nama Kebun</label>
											<input type="hidden" class="form-control" name="kd" value="<?= $kbn['id'] ?>" />
                                            <input type="text" class="form-control" name="kbn" value="<?= $kbn['nama_kebun'] ?>" readonly />
                                          </div>
										  <div class="form-group">
                                            <label>Jumlah Stok</label>
                                            <input type="text" class="form-control" name="stok" value="<?= round($kj['hasil'],2) ?>" readonly />
                                          </div>
										  <div class="form-group">
                                            <label>Bagus</label>
                                            <input type="text" class="form-control" name="bg"/>
                                          </div>
										  <div class="form-group">
                                            <label>Kurang Bagus</label>
                                            <input type="text" class="form-control" name="bkg"/>
                                          </div>
										  <div class="form-group">
										    <label>Blok</label>
											<select name="blok" id="blok" class="form-control">
												<option>--Pilih Blok--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM blok where id_kebun='$_GET[id_]'");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_blok']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
                                          <div class="form-group">
										    <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" cols="10" rows="8"></textarea>
                                          </div>                                         
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="hasil.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $tgl=$_POST['tgl'];
                                      $kd=$_POST['kd'];
                                      $bg=$_POST['bg'];
									  $bkg=$_POST['bkg'];
									  $blok=$_POST['blok'];
									  $ket=$_POST['ket'];
									  
									  $ttl = $bg+$bkg;
                                      

                                      if(isset($tgl,$bg)){
                                        if((!$tgl)||(!$bg)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO hasil_panen VALUES ('','$kd','$blok','$bg','$bkg','$ttl','$tgl','$ket',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=hasil.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				
				<br><br>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
								
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Bagus</th>
                                            <th>Kurang Bagus</th>
                                            <th>Total Panen</th>
											<th>Keterangan</th>
											<th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM hasil_panen where id_kebun='$_GET[id_]'");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['tanggal'];?></td>
                                            <td><?php echo $row['jumlah_bagus'];?></td>
                                            <td><?php echo $row['jumlah_KrgBagus'];?></td>
											<td><?php echo $row['total'];?></td>
											<td><?php echo $row['keterangan'];?></td>
                                        <?php

                                              
                                              print("
                                                <td>
												<a class='btn btn-warning' href=hasilEdit.php?id_=$row[id_kebun]>
                                                Update
                                                </a>
                                                <a class='btn btn-danger' href=hasilHapus.php?id_=$row[id_kebun]>
                                                Hapus
                                                </a>
                                 
                                                </td>
                                              </tr>");
                                              

                                              $no++;

                                        ?>
                                        </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>