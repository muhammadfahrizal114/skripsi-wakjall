<!DOCTYPE HTML>
<?php
    session_start();
	include 'dbconf.php';
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>StickyMobile BootStrap</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">

<div class="page-content header-clear-small">
<br><br>
<div class="" style="margin-top:12px;">
<div class="content mt-4 mb-0">
<center><a href="" onClick="document.location.reload(true)"><img src="images/logo/logoWisata.png" width="250px" height="250px" /></a></center><br>
<h1 class="text-center font-900 font-40 text-uppercase mb-0">Registrasi</h1><br><br><br>
<form method="post" target="_self">
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-user"></i>
<input type="text" class="form-control validate-name" name="nama" id="form1a" placeholder="Nama Lengkap" style="background-color: #fff0;">
<label for="form1a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Nama Lengkap</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-file"></i>
<select class="form-control validate-name" name="jk" style="background-color: #fff0;">
	<option value="">--- Jenis Kelamin ---</option>
	<option value="L">Laki - laki</option>
	<option value="P">Perempuan</option>
</select>
<label for="form1a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Jenis Kelamin</label>
</div>

<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-file"></i>
<input type="text" class="form-control validate-password" id="form3a" name="alamat" placeholder="Alamat" style="background-color: #fff0;">
<label for="form3a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Alamat</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-envelope"></i>
<input type="hidden" id="lat" name="lat" >
<input type="hidden" id="long" name="long">
<input type="text" class="form-control validate-name" name="user" id="form1a" placeholder="Username" style="background-color: #fff0;">
<label for="form1a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Username</label>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<i class="fa fa-lock"></i>
<input type="password" class="form-control validate-password" id="form3a" name="pass" placeholder="Password" style="background-color: #fff0;">
<label for="form3a" class="color-blue-dark font-10 mt-1" style="background-color: #fff0;">Password</label>
</div>
<button type="submit" class="btn btn-m mt-2 mb-4 btn-full bg-blue-dark text-uppercase font-900" style="width:100%">Register</button>
 <div class="d-flex">
 <a href="login.php">Sudah daftar, Login disini?</a>
</div>
</form>
</div>
</div>
<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
		$query = "SELECT max(No_Pendaftaran) as maxKode FROM peserta";
		$hasil = mysqli_query($mysqli,$query);
		$data = mysqli_fetch_array($hasil);
		$kodePeserta = $data['maxKode'];
		
		$noUrut = (int) substr($kodePeserta, 4, 4);
		
		$noUrut++;
		
		$char = "S-";
		$kodePeserta = $char . sprintf("%04s", $noUrut);
		echo $kodePeserta;
		
			$result = mysqli_query($mysqli, "insert into peserta values ('$kodePeserta','$_POST[user]','$_POST[pass]','$_POST[kelas]','$_POST[nama]','$_POST[jk]','$_POST[umur]','$_POST[alamat]','$_POST[kerja]','','')");
			$masuk1 = mysqli_query($mysqli, "insert into nilai values ('','$kodePeserta','','','','','')");
			$masuk2 = mysqli_query($mysqli, "insert into normalisasi values ('','$kodePeserta','','','','','')");
			if($result){
				echo "<script>alert('Anda berhasil mendaftar, login menggunakan email dan password yang anda daftarkan...')</script>";
				header('Location: login.php');
			}else { 
				header('Location: login.php');
			}
		
	}
?>
</div>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>
<script>
$(document).ready(function() {
var x = document.getElementById("demo");
var lat = document.getElementById("lat");
var long = document.getElementById("long");

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
 
 
 function showPosition(position) {
  lat.value = position.coords.latitude;
  long.value = position.coords.longitude;
}
});
</script>
</body>
