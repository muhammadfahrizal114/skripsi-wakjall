<!DOCTYPE HTML>
<?php
session_start();
include("connect.php");
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="theme-color" content="#000" />
<title>StickyMobile BootStrap</title>
<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
</head>
<body class="theme-light" data-highlight="highlight-red" data-gradient="body-default">
<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
<div id="page">
<div class="header header-fixed header-logo-center">
<a href="menuUtama.php" class="header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
<a href="#" class="header-title" style="left:49% !important">KELOLA KEUANGAN</a>
<a href="#" class="header-icon header-icon-4" onClick="document.location.reload(true)"><i class="fas fa-sync-alt"></i></a>
</div>
<div id="footer-bar" class="footer-bar-1">
<a href="utama.php" ><i class="fa fa-home"></i><span >Home</span></a>
<a href="menuUtama.php" class="active-nav"><i class="fas fa-th"  style="color:blue !important;"></i><span style="color:blue !important;">Menu</span></a>
<a href="profil.php"><i class="fas fa-user"></i><span>Profile</span></a>
</div>
<div class="page-content header-clear-medium">
<div class="row mb-0">
<?php

?>

<div class="col-6 pe-0" style="width:96.5%">
<div class="content mb-3">
<div class="d-flex" style="margin-top:10px;">
<a href="pemasukan.php" class="btn btn-m bg-blue-dark rounded-sm text-uppercase font-800" style="width:100%; height:20%"><img src="icon/pemasukan.png" width="50%" height="50%" />
<br><br><h1><b>Pemasukan</b></h1></a>
</div>
</div>
</div>

<div class="col-6 pe-0" style="width:96.5%">
<div class="content mb-3">
<div class="d-flex" style="margin-top:10px;">
<a href="pengeluaran.php" class="btn btn-m bg-blue-dark rounded-sm text-uppercase font-800" style="width:100%; height:20%"><img src="icon/pengeluaran.png" width="50%" height="50%" />
<br><br><h1><b>Pengeluaran</b></h1></a>
</div>
</div>
</div>





</div>
<div id="menu-login-1" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="350" data-menu-effect="menu-over">
<div class="menu-title mt-n1">
<h1>Tuliskan Pengaduan Anda...</h1>
<p class="color-theme opacity-50">isilah form dibawah ini..</p>
<a href="#" class="close-menu"><i class="fa fa-times"></i></a>
</div>
<form  method="post" target="_self" enctype="multipart/form-data">
<div class="content mb-0">
<div class="input-style no-borders has-icon mb-4">
<div class="color-blue-dark">Jenis Keluhan</div>
<select class="form-control" name="jenis">
<option value="">..:: Pilih Jenis Keluhan ::..</option>
<?php
$query = mysqli_query($konek,"select * from jenispengaduan");
while($dta = mysqli_fetch_array($query)){
?>
<option value="<?php echo $dta['kdJenis']; ?>"> <?php echo $dta['jenis']; ?> </option>
<?php
}
?>
</select>
</div>

<div class="input-style no-borders has-icon mb-4">
<div class="color-blue-dark">Keterangan</div>
<textarea name="keterangan" id="form1a" class="form-control" rows="5" cols="5"></textarea>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Surat Kehilangan ID Card</div>
<input type="file" name="file1" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KTP</div>
<input type="file" name="file2" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KK</div>
<input type="file" name="file3" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Slip Gaji</div>
<input type="file" name="file4" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Time Sheet</div>
<input type="file" name="file5" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">KTA</div>
<input type="file" name="file6" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Ijazah Diksar</div>
<input type="file" name="file7" class="form-control" />
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
<div class="color-blue-dark">Ijazah Diksar</div>
<input type="file" name="file8" class="form-control" />
</div>
<div class="row">
<div class="col-6">
</div>
<div class="col-6">
<div class="clearfix"></div>
</div>
</div>
<div class="input-style no-borders has-icon validate-field mb-4">
</div>
<br>
<button type="submit" class="btn btn-full btn-m shadow-l rounded-s text-uppercase font-900 bg-blue-dark mt-n2" style="width:100%"><i class="fa fa-search"></i> Kirim</button>
<br>
</form>
</div>
</div>
<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$targetDir = "uploads/";
			$fileName1 = basename($_FILES["file1"]["name"]);
			$targetFilePath1 = $targetDir . $fileName1;
			
			$fileName2 = basename($_FILES["file2"]["name"]);
			$targetFilePath2 = $targetDir . $fileName2;
			
			$fileName3 = basename($_FILES["file3"]["name"]);
			$targetFilePath3 = $targetDir . $fileName3;
			
			$fileName4 = basename($_FILES["file4"]["name"]);
			$targetFilePath4 = $targetDir . $fileName4;
			
			$fileName5 = basename($_FILES["file5"]["name"]);
			$targetFilePath = $targetDir . $fileName5;
			
			$fileName6 = basename($_FILES["file6"]["name"]);
			$targetFilePath6 = $targetDir . $fileName6;
			
			$fileName7 = basename($_FILES["file7"]["name"]);
			$targetFilePath7 = $targetDir . $fileName7;
			
			$fileName8 = basename($_FILES["file8"]["name"]);
			$targetFilePath8 = $targetDir . $fileName8;
			
			
			
			
			move_uploaded_file($_FILES["file1"]["tmp_name"], $targetFilePath1);
			move_uploaded_file($_FILES["file2"]["tmp_name"], $targetFilePath2);
			move_uploaded_file($_FILES["file3"]["tmp_name"], $targetFilePath3);
			move_uploaded_file($_FILES["file4"]["tmp_name"], $targetFilePath4);
			move_uploaded_file($_FILES["file5"]["tmp_name"], $targetFilePath5);
			move_uploaded_file($_FILES["file6"]["tmp_name"], $targetFilePath6);
			move_uploaded_file($_FILES["file7"]["tmp_name"], $targetFilePath7);
			move_uploaded_file($_FILES["file8"]["tmp_name"], $targetFilePath8);
			
						
			$result = mysqli_query($konek, "insert into pengaduan values('',
																		 '".$_POST['jenis']."',
																		 NOW(),
																		 '".$_GET['no']."',
																		 '".$_POST['keterangan']."',
																		 '".$fileName1."',
																		 '".$fileName2."',
																		 '".$fileName3."',
																		 '".$fileName4."',
																		 '".$fileName5."',
																		 '".$fileName6."',
																		 '".$fileName7."',
																		 '".$fileName8."',
																		 '0')");
						
			header('Location: pengaduan.php');
			}
	
?>

<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script src="scripts/jQuery-2.2.0.min.js"></script>

</script>
</body>
