<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Transaksi Penjualan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Transaksi</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
				  <?php
					$char = date("Ymd");
					$query=mysqli_query($konek,"SELECT max(kode_transaksi) as max_kode FROM transaksi 
					WHERE kode_transaksi LIKE '{$char}%' ORDER BY kode_transaksi DESC LIMIT 1");
					$data = mysqli_fetch_array($query);
					$kodeTransaksi = $data['max_kode'];
					$no = substr($kodeTransaksi, -3, 3);
					$no = (int) $no;
					$no += 1;
					$newKodeTransaksi = $char .'/'. sprintf("%03s", $no);
				   ?>
										  <div class="form-group">
                                            <label>Kode Transaksi</label>
                                            <input type="text" class="form-control" name="kode" value="TRX/<?php echo $newKodeTransaksi; ?>" readonly />
                                          </div>
										  <div class="form-group">
										    <label>Kebun</label>
											<select name="kebun" id="kebun" class="form-control">
												<option>--Pilih Kebun--</option>
												<?php
												$sqql = mysqli_query($konek,"SELECT * FROM kebun");
												while ($rorow = mysqli_fetch_array($sqql)) { 
												?>
												<option value="<?php echo $rorow['id'] ?>"><?php echo $rorow['nama_kebun']; ?></option>
												<?php												
												}
												?>
										   </select>
                                          </div>
										  <div class="form-group">
										    <label>Nama Pembeli</label>
                                            <input type="text" class="form-control" name="nama" />
                                          </div>
										  <div class="form-group">
										    <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl" />
                                          </div>
										  <div class="form-group">
										    <label>Berat Bagus</label>
                                            <input type="text" class="form-control" name="bg" />
                                          </div>
                                          <div class="form-group">
										    <label>Berat Kurang Bagus</label>
                                            <input type="text" class="form-control" name="bkg" />
                                          </div>
										 
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="pemasukan.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      
									  $kd=$_POST['kode'];
                                      $kebun=$_POST['kebun'];
                                      $nama=$_POST['nama'];
                                      $tgl=$_POST['tgl'];
									  $bg=$_POST['bg'];
									  $bkg=$_POST['bkg'];
                                      
								      $hrg = mysqli_fetch_array(mysqli_query($konek,"select * from harga where id_kebun='$kebun'"));
									  $hrg_bg = $bg * $hrg['harga_Bagus'];
									  $hrg_bkg = $bkg * $hrg['harga_KrgBagus'];
								      $jml_bayar = ($hrg_bg + $hrg_bkg);
									  $ttl_disk = $jml_bayar;
									  $total = $jml_bayar + $ttl_disk;
									
									  
                                      if(isset($nama,$kebun)){
                                        if((!$nama)||(!$kebun)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO transaksi VALUES ('','$kd','$nama','0','$total','$total','$tgl',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);
									  $idtrans = mysqli_fetch_array(mysqli_query($konek,"select * from transaksi where kode_transaksi='$kd'"));
									  $query = mysqli_query($konek,"INSERT INTO detail_transaksi VALUES ('','$kebun','$idtrans[id]','$hrg_bg','$hrg_bkg','$total','',NOW(),NOW())");
                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=pemasukan.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>