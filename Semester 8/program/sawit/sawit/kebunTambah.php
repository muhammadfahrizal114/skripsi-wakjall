<?php
  include "header.php";
include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">TAMBAH DATA KEBUN</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Tambah Kebun</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
                                           
                                          <div class="form-group">
                                            <label>Nama Kebun</label>
                                            <input type="text" class="form-control" name="nama"/>
                                          </div>
										  <div class="form-group">
                                            <label>Panjang</label>
                                            <input type="text" class="form-control" name="pjg"/>
                                          </div>
										  <div class="form-group">
                                            <label>Lebar</label>
                                            <input type="text" class="form-control" name="lbr"/>
                                          </div>
										  <div class="form-group">
                                            <label>Luas</label>
                                            <input type="text" class="form-control" name="luas"/>
                                          </div>
                                          <div class="form-group">
										    <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" cols="10" rows="8"></textarea>
                                          </div>
                                        
                                          
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="kebun.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      

                                      $nama=$_POST['nama'];
                                      $pjg=$_POST['pjg'];
                                      $lbr=$_POST['lbr'];
									  $luas=$_POST['luas'];
									  $ket=$_POST['ket'];
                                      

                                      if(isset($nama,$pjg)){
                                        if((!$nama)||(!$pjg)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

                                     
                                      $add_kelas="INSERT INTO kebun VALUES ('','$nama','$pjg','$lbr','$luas','$ket',NOW(),NOW())";
                                      mysqli_query($konek,$add_kelas);

                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=kebun.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>

<?php
  include "footer.php";
?>