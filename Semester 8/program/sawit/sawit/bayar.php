<?php
  include "header.php";
  include "menu.php";
  ini_set("display_errors","Off");
  include("connect.php");
?>



<div class="page-wrapper">
            
            <div class="container-fluid">
                
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Transaksi Penjualan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Transaksi</li>
                        </ol>
                    </div>
                   
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                  <form action="" method="post" enctype="multipart/form-data" id="frm-mhs">
				  <?php
				  $row = mysqli_fetch_array(mysqli_query($konek,"select * from transaksi where id='$_GET[id_]'"));
				  $sqlb=mysqli_query($konek,"SELECT SUM(bayar) as total FROM pembayaran where id_transaksi='$_GET[id_]'");
				  $bay = mysqli_fetch_array($sqlb);
				  $query=mysqli_query($konek,"SELECT SUM(harga) as  total1 FROM detail_transaksi where id_transaksi='$_GET[id_]'");
				  $ttl = mysqli_fetch_array($query);
				  $sisa = $ttl['total1'] - $bay['total'];
				  ?>
										  <div class="form-group">
                                            <label>Kode Transaksi</label>
											<input type="hidden" name="id_" value="<?php echo $_GET['id_']; ?>" />
                                            <input type="text" class="form-control" name="kode" value="<?php echo $row['kode_transaksi']; ?>" readonly />
                                          </div>
										  <div class="form-group">
										    <label>Nama Pembeli</label>
                                            <input type="text" class="form-control" name="nama" value="<?php echo $row['pembeli']; ?>" readonly />
                                          </div>
										  <div class="form-group">
										    <label>Sisa</label>
                                            <input type="text" class="form-control" name="sisa" value="<?php echo $sisa; ?>" readonly />
                                          </div>
										  <div class="form-group">
										    <label>Tanggal</label>
                                            <input type="date" class="form-control" name="tgl" />
                                          </div>
										  <div class="form-group">
										    <label>Bayar</label>
                                            <input type="text" class="form-control" name="bay" />
                                          </div>
                                          <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Simpan" />
                                            <a class="btn btn-warning" href="pemasukan.php">Kembali</a>
                                          </div>
                                          
                                        </form>

                              <?php
                                      
									  $kd=$_POST['id_'];
									  $tgl=$_POST['tgl'];
									  $bayar=$_POST['bay'];
                                       
                                      if(isset($tgl,$bayar)){
                                        if((!$tgl)||(!$bayar)){
                                        print "<script>alert ('Harap semua data diisi...!!');</script>";
                                        print"<script> self.history.back('Gagal Menyimpan');</script>"; 
                                        exit();
                                        } 

									  $query = mysqli_query($konek,"INSERT INTO pembayaran VALUES ('','$kd','$bayar','$tgl',NOW(),NOW())");
                                      echo '
                                      <script type="text/javascript">
                                       
                                             alert ("Data Berhasil Ditambah!");
                                             
                                      </script>
                                      ';
                                      echo '<meta http-equiv="refresh" content="1; url=pemasukan.php" />';


                                      } 

                                ?>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                  
                 
                                <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah Dibayar</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php

                                        $sql=mysqli_query($konek,"SELECT * FROM pembayaran where id_transaksi='$_GET[id_]'");

                                        $no=1;

                                        while ($row=mysqli_fetch_array($sql)){
										?>

                                          <tr class='td' bgcolor='#FFF'>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row['tanggal'];?></td>
                                            <td><?php echo $row['bayar'];?></td>
                                        
                                          </tr>
                                        <?php }?>
                                        

                                    </tbody>
                                </table>
                            </div>
                  
                  
                            </div>
                        </div>
                    </div>
                </div>
				
				
                
            </div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#frm-mhs').validate({
        rules: {
          nama_kriteria : {
            minlength:2,
            required:true
          }
        },
        messages: {
          nama_kriteria: {
            required: "* Kolom nama kriteria harus diisi",
            minlength: "* Kolom nama kriteria harus terdiri dari minimal 2 digit"
          }
        }
      });
    });
    
    
    </script>
<script type="text/javascript">
            $(function() {
                $("#datatable").dataTable();
				$("#datatable1").dataTable();
            });
        </script>    
<?php
  include "footer.php";
?>