<?php
include "header.php";
include "menu.php";
?>

    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                   
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                               

                  <h1 class="mt-5"><p align="center">Halaman Utama Administrator</p></h1>
		          <p class="lead" align="center">
		          	SISTEM INFORMASI PEREKBUNAN SAWIT
					
		          </p>
		          <p class="lead" align="center"><img src="images/muris-studio.jpg" align="center" width="150"></p>
                    
                  

                  <table class="table table-sm" align="left">
                         <tr>
                          <td>Nama</td>
                          <td>: <?php echo ''.$_SESSION[nama].'';?></td>
                        </tr>
                        <tr>
                          <td>NIK</td>
                          <td>: <?php echo ''.$_SESSION[nik].'';?></td>
                        </tr>
                        <tr>
                          <td>No. telepon</td>
                          <td>: <?php echo ''.$_SESSION[telepon].'';?></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td>: <?php echo ''.$_SESSION[email].'';?></td>
                        </tr>
                        <tr>
                          <td>Login Sebagai</td>
                          <td>: 
                            Administrasi</td>
                        </tr>
                        
                        </table>

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
 <?php
include "footer.php";
?>           